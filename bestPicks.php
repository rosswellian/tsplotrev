﻿<?php

$dbg = 0;

// default mode is npick meaning pick N equally-spaced traces
// TODO: use SNRs to help decide best, when multiple nearby choices
function bestDists($dists, $snrs, $mode = "npick", $npicks = 10, $pickgap = 15) {

	global $dbg;

    if(!isSet($dists) || gettype($dists) != "array") {
         echo "error: bestDists : dists array numeric values expected\n";
         return(array());
    }
    if(count($dists) < 10)   {
         echo "error: bestDists: meant for dists array with 10 or more values\n";
         return(array());
    }
    if($mode == "npick" && ($npicks < 3 || $npicks > 40)) {
         echo "error: bestDists: npick mode needs npicks from 3 to 40 \n";
         return(array());
    }
    if($mode == "gappick" && ($pickgap < 3 || $pickgap > 20)) {
         echo "error: bestDists: gappick mode needs pickgap from 3 to 20 \n";
         return(array());
    }

    sort($dists); // min is now first and max last
    //if($dbg) echo "sorted dists:\n";
	//if($dbg) var_dump($dists);

    $picksIndices = array();

    if($mode == "npick") {

        // compute degree span we are going to cover:

        //$minDist = array_min($dists);
        $minDist = $dists[0]; // is equivalent to array_min
        if($dbg) echo "minDist is $minDist\n";

        //$maxDist = array_max($dists);
        $maxDist = $dists[count($dists) - 1]; // equivalent to array_max
        if($dbg) echo "maxDist is $maxDist\n";

        $degSpan = $maxDist - $minDist;
        if($dbg) echo "the search spans $degSpan degrees...\n"; 

        // we have chosen two of the desired npicks already, first and last,
        // so divide the span into (npicks-2) equal intervals

        $degInterval = round($degSpan / ($npicks - 1));
        if($dbg) echo "so we search at degree interval:  $degInterval ...\n"; 

        array_push($picksIndices, 0); // first is first
        //array_shift($dists);
        if($dbg) echo "added min(dists) as 0 to picksIndices\n";


        for($p = 1; $p < $npicks - 1; $p++) {

            //$want = $minDist + $p * $degInterval;  // original way, works but can end up w/very close picks

            // test: if a pick was made  (after first time) then set next pick degInterval away from it:
            if($p == 1) 
                $want = $minDist + $degInterval;
            else 
                $want = $dists[$closestIndex] + $degInterval; // a pick was already made let's bum $want





            if($dbg) echo "searching near $want degs: ";
            $closest = 999;
            $closestIndex = -1;
            for($d = 0; $d < count($dists); $d++) {
                //if($dbg) echo "comparing $dists[$d] to $want\n";
                $close = abs($dists[$d] - $want);
                if($close < $closest) {
                    $closestIndex = $d;
                    $closest = $close;
                    //if($dbg) echo "new closest of $closest at index $closestIndex \n";
                }
            }
            if($closest != 999) {
                if(array_search($closestIndex, $picksIndices) == FALSE ) {
                    if($dbg) echo "closest to $want is $dists[$closestIndex] at index $closestIndex to picksIndices\n";
                    array_push($picksIndices, $closestIndex);
                }
                else 
                    if($dbg) echo "skipping, closest to $want was $dists[$closestIndex] at index $closestIndex, and was already found \n";
            }
            else {
              if($dbg) echo "warning: nothing at all close found\n";
            }
        }
        array_push($picksIndices,  count($dists) - 1); 
        if($dbg) echo "added max(dists) to picksIndices:\n";
    }

    return($picksIndices);
}


/*
$staDists=array(4,6,12,23,25,25.5,30,32,34,35,39,41,42,44,49,50,53,55,56,60,63,64.5,65,69,70,71,73,80,84,88,90,91,98,102,106,110,120,123,127,131,140,148,150,158,164);

$staSNRs=array(402,602,1202,2302,2402,25,25.502,30,3202,34,3502,39,4102,42,4402,49,5002,53,5502,56,65002,653,6402,694.5,602,699,7002,71,732,80,8402,88,902,91,9802,102,1002,110,12002,223,12702,131,140,1480202,15002,1582,164);

//sort($staDists);
 if($dbg) {
	echo "the dists are: \n";
 	print_r($staDists);
 	echo "the SNRs are: \n";
 	print_r($staSNRs);
}

$ntoPick = 10;
$picksIndices = bestDists($staDists, $staSNRs, "npick", $ntoPick );

echo "the best dists for picking $ntoPick are: \n";
foreach($picksIndices as $pick)
	echo "$staDists[$pick],";
echo "\n";

 */

?>

