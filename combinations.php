﻿<?php


if (!function_exists('stats_standard_deviation')) {
    /**
     * This user-land implementation follows the implementation quite strictly;
     * it does not attempt to improve the code or algorithm in any way. It will
     * raise a warning if you have fewer than 2 values in your array, just like
     * the extension does (although as an E_USER_WARNING, not E_WARNING).
     *
     * @param array $a
     * @param bool $sample [optional] Defaults to false
     * @return float|bool The standard deviation or false on error.
     */
    function stats_standard_deviation(array $a, $sample = false) {
        $n = count($a);
        if ($n === 0) {
            trigger_error("The array has zero elements", E_USER_WARNING);
            return false;
        }
        if ($sample && $n === 1) {
            trigger_error("The array has only 1 element", E_USER_WARNING);
            return false;
        }
        $mean = array_sum($a) / $n;
        $carry = 0.0;
        foreach ($a as $val) {
            $d = ((double) $val) - $mean;
            $carry += $d * $d;
        };
        if ($sample) {
           --$n;
        }
        return sqrt($carry / $n);
    }
}






//$uniqs = array();

ini_set('memory_limit','1000M');

// shit!  "gap" is not "num traces" -- confusion here!


// a bunch of globals needed inside combinations:
$bestAvgSNR = -1.; // less important than regular spacing
$bestFoundGap = -1; // find it by minimizing stdev
$lowestStdev = 99999.;

// 10 is arbitrary for now
$bestAvgSNRForGaps = array();
$traceidsForGaps = array();
$gapsForGaps = array();

$bestStdevForGaps = -1.;

function combinations($dists, $len, $startPosition, $resultAry){
 	global $traceidsAry;
 	global $distsAry;
 	global $snrsAry;
 	global $bestAvgSNRForGaps;
 	global $traceidsForGaps;
 	global $gapsForGaps;

 	global $lowestStdev;

    //print("dists: ");
    //print_r($dists );
    //print("len:$len\n");
    //print("start at:$startPosition\n");
    //print("resultAry: ");
    //print_r($resultAry );

    if ($len == 0) {
        //echo "got a combination resultAry :\n";
        //echo join(",", $resultAry) . "\n";


           $theseIDs = array_map(
					function($key) use ($traceidsAry){
							return $traceidsAry[$key]; },
					$resultAry);
           $theseDists = array_map(
					function($key) use ($distsAry){
							return $distsAry[$key]; },
					$resultAry);
/*
if(min($theseDists) > 12.) 
    return;
if(max($theseDists) < 140.) 
    return;
*/

           $theseSNRs =  array_map(
					function($key) use ($snrsAry) {
							return $snrsAry[$key]; },
					$resultAry);

            $dist = $theseDists[0]; // first dist is first gap
            $prevDist = $dist;
            $theseGaps = array();

			// spec. case: first one has no "prev" dist, so
			// the gap between it and the second dist is same,
			// or the distance from zero to it...
			//array_push($theseGaps, $theseDists[0]);

            for($d = 1; $d < count($theseDists); $d++) {
                $dist = $theseDists[$d];
                $thisGap = round($dist - $prevDist, 1);
            	$prevDist = $dist;
				array_push($theseGaps, $thisGap);
			}

if(min($theseGaps) < 2.)
    return;
if(max($theseGaps) > 40.)
    return;

echo ".";


           $thisAvgSNR = array_sum($theseSNRs) / count($theseSNRs);
           $thisAvgGap = round(array_sum($theseGaps) / count($theseGaps), 1);

$thisStdev = stats_standard_deviation($theseGaps);
if($thisStdev < $lowestStdev) {
    echo "$thisStdev is lower than $lowestStdev at gaps: " . join(",",$theseGaps) . "with avg gap of " . $thisAvgGap . "\n";
    $lowestStdev = $thisStdev;
}
else
    return;
//echo "theseGaps: " . join(",", $theseGaps) . "  have stdev of: " . $thisStdev . "\n";

//echo "$thisAvgGap, ";

            if($thisAvgSNR >  $bestAvgSNRForGaps["$thisAvgGap"] ) { // only if trace gaps near 10
                $bestAvgSNRForGaps["$thisAvgGap"]  = $thisAvgSNR;
		        $traceidsForGaps["$thisAvgGap"] = join(",", $theseIDs);
		        $gapsForGaps["$thisAvgGap"] = join(",", $theseGaps);

echo "\n>>> new best avg SNR for gap $thisAvgGap is $thisAvgSNR\n";

//echo "\n\n";
//echo "theseIDs: " . join(",", $theseIDs) . "\n";
//echo "theseDists: " . join(",", $theseDists) . "\n";
//echo "theseSNRs: " .  join(",", $theseSNRs) . "\n";
//echo "theseGaps: " . join(",", $theseGaps) . "\n";
//echo "\n";
			}

        return; // unwind recursion
    } // end len==0, process a combination

    // else: RECURSE!
    for ($i = $startPosition; $i <= count($dists) - $len;  $i++){
        $resultAry[count($resultAry) - $len] = $dists[$i];
        $c = combinations($dists, $len - 1, $i + 1, $resultAry);
        //print_r($c);
    }
  	return;
}

// return the gap assoc'd with a key
function lookup($index,$dists) {
    return $dists[$index];
}

// NOT USED NOW:
// make sure a combination includes both first and last dists(s)
function isAnchoredAtEnds($combo) {
    global $indices;
	echo "isAnchored?: $combo[0]  $indices[0]  ";
 	if($combo[0] == $indices[0] &&
 	   $combo[count($combo)-1] == $indices[count($indices)-1]) {
			echo "yes\n";
			return true;
		}
	else  {
			echo "no\n";
			return false;
		}
}




//function bestIndicesComboForGap($ids, $dists, $snrs, $gap, $bestNumTraces = 10, $minNumTraces = 8, $maxNumTraces = 12) {
function bestIndicesComboForGap($ids, $dists, $snrs, $bestNumTraces = 10, $minNumTraces = 8, $maxNumTraces = 12) {

	global $traceidsAry;
	global $distsAry;
	global $snrsAry;
	$traceidsAry = $ids;
	$distsAry = $dists;
	$snrsAry = $snrs;

	global $bestAvgSNRForGaps;
	global $traceidsForGaps;
	global $gapsForGaps;

    // these will be passed to combinations():
    $indices = range(0,count($distsAry)-1);
	$rslt = array_pad(array(), $bestNumTraces, $indices);

//var_dump($indices);
//var_dump($rslt);

	// for now only considering the best number of indices, e.g. 10
	combinations($indices, $bestNumTraces, 0, $rslt); // $uniqs is set here


echo "dists and snrs arrays:\n";
var_dump($distsAry);
var_dump($snrsAry);

echo "resutls:\n";
var_dump($bestAvgSNRForGaps );
var_dump($gapsForGaps );
var_dump($traceidsForGaps );
}





/*


// for now, array of letters represents gaps for testing.
global $distsAry;
$distsAry = array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","a","b","c","d","e","f","g","h","i","j","k","l","m","n");

// indices are indices into distsAry array
$indices = range(0,count($distsAry)-1);

for($ngaps = 10; $ngaps <= 10; $ngaps++) {
	$uniqs = array(); // is a global, necessary for recursion in combinations()
	$rslt = array_pad(array(), $ngaps, $indices);

	combinations($indices, $ngaps, 0, $rslt);

	$anchoredUniqs = array_filter($uniqs, 'isAnchoredAtEnds');

	$c = 1;
	foreach($anchoredUniqs as $u) {
 		//print_r($u);
 		$stas = array_map('lookup', $u,
                       		array_fill(0, count($u),
							$gaps));
    	//echo "\n this ($ngaps gaps) unique combo is: ";
    	echo "$ngaps gaps, combo $c:  ";
		foreach($stas as $s)
 			echo("$s,");
 		echo("\n");

    	echo "$ngaps indices , combo $c:  ";
		foreach($u as $k)
 			echo("$k,");
 		echo("\n");
 		echo("\n");
 		echo("\n");

 		$c++;
	}
 	print("\n\n");
}

*/

?>


