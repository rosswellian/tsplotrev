#!/bin/ksh

# Global Seismogram Plotter cron job

# load any new earthquakes into sqlite database
# fetch the trace data for them from web services
# score all the plots in database

# purpose is to update map.phtml with new (red) quakes

# runs every few hours as a cron job

# R.Welti  1/16/18

echo  >>cron.log
echo  >>cron.log
echo  >>cron.log
echo cron running at `date` >> cron.log

dir=`pwd`
echo current directory is $dir >>cron.log

if [[ "$dir" != "/opt/seismon/genmap_new/sm2/recsec" ]] ; then
   echo "error: must be run from script dir" >>cron.log
   exit -1
fi

log=`stat -c %s cron.log`

if [[ "$log" != "" ]] ; then
    echo log file is $log bytes big >>cron.log
    if (( $log > 70000000 )) ; then
        echo log file is too big so compressing >>cron.log
        /bin/gzip -fq cron.log
        echo log file was too big so just compressed it >>cron.log
    fi
else
    echo log file did not exist will create it >>cron.log
fi

# get the last 2 days of mag-eligible quakes

# update.php [dbname timeperiod]  defaults: plotsdb tminus_2days_ws

php ./update_db.php >> cron.log

# die(), in php in command line CLI mode, returns 0 - so for now use
# last line in file to determine if succeeded

lastline=`tail -1 cron.log 2>/dev/null`
#echo $lastline

if [[ "$lastline" = "" || $lastline != *new\ events ]] ; then
   echo "exiting due to no new events, or update_db.php failure" >>cron.log
   exit
fi


echo "running php ./getTraces.php refresh 2" >> cron.log
php ./getTraces.php refresh 2 >> cron.log
echo  >>cron.log
echo  >>cron.log

echo "running php ./scorePlots.php " >> cron.log
php ./scorePlots.php >> cron.log
echo  >>cron.log
echo  >>cron.log

echo cron finished at `date`  >>cron.log
echo  >>cron.log
echo  >>cron.log
echo  >>cron.log
