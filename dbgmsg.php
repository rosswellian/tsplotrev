<?php

/*
 * Simple debug message support
 */

/**
 * global debug control for all files that use dbgmsg()
 */
$dbg = false; 
if(isset($_GET['dbg'])) {
    global $dbg;
    echo "resetting dbg to true\n";
    $dbg = true; 
}

/**
 * @global boolean controls if prints
 * @param string the message to print
 */
function dbgmsg($msg) {
    global $dbg;
    if ($dbg)
        print($msg . "\n");
        //else echo "dbg is false\n";
}

?>
