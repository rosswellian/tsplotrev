#!/bin/ksh

if [[ $# -eq 0 ]];then
   print "usage: evInfo.ksh [evid | latest]"
   exit
fi

if [[ $1 != latest ]] ; then
    /usr/bin/sqlite3 plotsdb << EOF
    select * from events where evid = $1;
EOF
else
    /usr/bin/sqlite3 plotsdb << EOF
    select * from events,plots where events.evid = plots.evid and plots.usable = 1 order by evid desc limit 1;
EOF
fi
