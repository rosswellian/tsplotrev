<?php

require_once "dbgmsg.php";

//global $dbg; // is bein reset in closestStation????
$dbg = false; // override setting from dbgmsg.php

error_reporting(E_ALL ^ E_NOTICE);
date_default_timezone_set("UTC");

$local_time = trim(`/bin/date`);
dbgmsg("$argv[0] running at $local_time ");


//$_GET['usableOnly'] = 1;
if(!isSet($_GET['usableOnly']) || $_GET['usableOnly'] == 1 )

    //  only emit events which are usable in plots table
    //$query = "select distinct events.*,plots.* from events inner join plots on events.evid = plots.evid and plots.usable = 1 group by events.evid limit 1";
    $query = "select distinct events.*, plots.* from events inner join plots on events.evid = plots.evid and plots.usable = 1 group by events.evid ";

else

    //  emit all events which are in plots table
    //$query = "select distinct events.* from events inner join traces on events.evid = traces.evid group by events.evid";
    $query = "select distinct events.*, plots.usable, plots.usableReason from events inner join plots on events.evid = plots.evid  group by events.evid ";


dbgmsg("opening database ...");

///////////////////////////// connect to database

$db_file = "plotsdb";
$data_dir = "/opt/seismon/gsv/data";
$db_dsn =  "sqlite:$db_file";

try {
      /*************************************
      * open connection                    *
      *************************************/
      dbgmsg( "opening $db_dsn...");
      $file_db = new PDO($db_dsn);
      $file_db->setAttribute(PDO::ATTR_TIMEOUT, 5);  // 5 seconds
}
catch(PDOException $e) {
        // Print PDOException message
        echo "\nError: failed to open connection to $db_file\n";
        die($e->getMessage());
}

$file_db->setAttribute(PDO::ATTR_ERRMODE,
                       PDO::ERRMODE_EXCEPTION);
$file_db->setAttribute(PDO::ATTR_TIMEOUT, 5);  // seconds


try {
    $result = $file_db->query($query);
}
catch(PDOException $e) {
     die("error: failed with $query on $db_file " .  $e->getMessage());
}
    //var_dump($result);
try {
    $events = $result->fetchAll (PDO::FETCH_NUM); // simple array, numeric only
    //$events = $result->fetchAll (PDO::FETCH_BOTH); // simple array, numeric only
    //var_dump($events);
    //exit;
}
catch(PDOException $e) {
    print("warning: could not fetchAll results on $query " . $e->getMessage() . " skipping");
    continue;
}
foreach ($events as $ev ) {

    $evid = $ev[0];
    if(file_exists("$data_dir/$evid")) {
        print(implode('|', $ev));
        print("\n");
    }

    //var_dump($ev);
}

?>
