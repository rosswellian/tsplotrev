#!/usr/bin/php
<?php

require_once "station.php";
require_once "getDistance.php";
require_once "longitude.php";

require_once "dbgmsg.php";

// if something notable changes, such as channel, timespan, min mag, filtering, etc
// we will need to rebuild the whole database and fetch new data. takes many hours!
// but once those things are stable we only go after recent traces, new events

// refresh: 
//    time is in days (default 30), 
//    runs fast, 
//    suitable for a cron job
//    operates on the live plotsdb file 
// rebuild: 
//    takes many hours, 
//    deletes existing data first, 
//    done rarely and manually
//    operates on the plotsdb_tmp file not the live plotsdb file
// repairID: 
//    operates on the live plotsdb file , only on given evid
//    suitable for dev work
//    deletes existing data first


//global $dbg; // is bein reset in closestStation????
$dbg = true; // override setting from dbgmsg.php

//error_reporting(E_ALL ^ E_NOTICE);
error_reporting(E_ALL );
date_default_timezone_set("UTC");

if(isSet($argv[1]))
	$mode = $argv[1];
else
	$mode = ""; // no default value currently

if($mode !== 'rebuild' && $mode !== 'refresh' && $mode != 'repairID')
	die("usage: $argv[0] [rebuild | refresh [daysAgo]] || repairID <eventID>");

$local_time = trim(`/bin/date`);
print("$argv[0] running at $local_time ");

$outdir = "/var/www/tmp/recsec/";  
if(!is_writable($outdir)) die("Error: $outdir does not exist or can't write to it\n");


///////////////////////////// connect to database

if($mode === "rebuild") { 
   // use tmp database just created by rebuild_db.ksh
   $db_file = "plotsdb_tmp";
   print("this will totally rebuild $db_file with trace data, and create new JSON and img files in /data/ dir; it takes a long time, like 10-15 hrs\n");
   $ok = readline("press return to continue or ctrl-C ");
}
elseif ($mode === "refresh") {
   // refresh operates on live database, runs pretty fast
   $db_file = "plotsdb";
   if(isSet($argv[2])) 
	   $daysAgo = intval($argv[2]);
   else
	   $daysAgo = 30;
   if(is_int($daysAgo)) echo "$daysAgo is int ";
   if(is_int($daysAgo ) && $daysAgo > 1 && $daysAgo < 72)
	  $daysAgo = "-" . $daysAgo . " days";
   else
	  $daysAgo = "-30 days ";
   print("getting recent events for now $daysAgo ...\n");
   print("this will operate on $db_file and take not long, mins not hours\n");
}
else {  // repairID mode
   $db_file = "plotsdb";
   if(isSet($argv[2])) {
	  $repairID = $argv[2];
      print("this will refetch traces for event ID $repairID \n");
      $ok = readline("press return to continue or ctrl-C ");
   }
   else 
      die("repairID requires a valid event ID");
}

$db_dir = "/opt/seismon/gsv";
$db_dsn =  "sqlite:$db_file";

try {
      /*************************************
      * open connection                    *
      *************************************/
      echo "opening $db_dsn...\n";
      $file_db = new PDO($db_dsn);
      $file_db->setAttribute(PDO::ATTR_TIMEOUT, 5);  // 5 seconds
}
catch(PDOException $e) {
        // Print PDOException message
        echo "\nError: failed to open connection to $db_file\n";
        die($e->getMessage());
}

$file_db->setAttribute(PDO::ATTR_ERRMODE,
                       PDO::ERRMODE_EXCEPTION);
$file_db->setAttribute(PDO::ATTR_TIMEOUT, 12);  // 12 seconds

if($dbg) {
       try {
          $result = $file_db->query("PRAGMA table_info(events)");
       }
       catch(PDOException $e) {
          echo "\nError: failed to query $db_file \n";
          die($e->getMessage());
       }
       //var_dump($result);
       $cols = $result->fetchAll (PDO::FETCH_BOTH);
       //var_dump($cols);

       $num = count ($cols);
       echo "Sanity: number of columns in EVENTS table = " . $num . "\n\n";
       for ($i=0; $i<$num; $i++) {
          $numflds = max(array_keys($cols[$i]));
          for ($j=0; $j<$numflds; $j++) {
             echo $cols[$i][$j] . " ";
             }
          echo "\n";
       }
}


//$query = "select evid, time, lat, lon, lon360, depth, mag, region from events    limit 1";   ///////////////
if($mode === "rebuild") 
    $query = "select evid, time, lat, lon, lon360, depth, mag, region from events  ";
elseif ($mode === "repairID") 
    $query = "select evid, time, lat, lon, lon360, depth, mag, region from events where evid = $repairID";
else 
    $query = "select evid, time, lat, lon, lon360, depth, mag, region from events where time > date('now', '$daysAgo') ";


echo $query;

try {
    $result = $file_db->query($query);
}
catch(PDOException $e) {
     die("error: failed with $query on $db_file " .  $e->getMessage());
}
//var_dump($result);

try {
    $events = $result->fetchAll (PDO::FETCH_BOTH); // BOTH means numeric and hash array indexes BOTH are there!
    var_dump($events);
}
catch(PDOException $e) {
    print("warning: could not fetchAll results on $query " . $e->getMessage() . " skipping");
    continue;
}

foreach ($events as $ev ) {
    dbgmsg("does event " . $ev["evid"] . " have any traces??\n");

    var_dump($ev);
    // if we don't have any traces, or if we need to recompute them
    // query the traces table for existence of traces for this event
    // OR
    // we may have new stations with new traces to add
    //
    $start = $ev["time"];
    try {
       $startEpoch = strtotime($start); // will add time below
    }
    catch (Exception $e) {
       print("warning: could not parse $start as valid date . skipping\n");
       continue;
    }

    // find out which GSN stations have broadband data for our event and time

    $mins = 30;  // make mins a GET param? prob. not worth it
    $end = date('Y-m-d\TH:i:s', $startEpoch + floor($mins * 60)); // end in UNIX seconds

    $evlat = $ev["lat"];
    $evlon = $ev["lon"];
    $evdep = $ev["depth"];

    $dataAvail = "http://service.iris.edu/fdsnws/station/1/query?net=_GSN-BROADBAND,_US-REF" .
        "&loc=00&cha=BHZ&starttime=${start}&endtime=${end}&lat=${evlat}&lon=${evlon}" .
        "&minradius=0.0&maxradius=180.0&level=station&includeavailability=true" .
        "&matchtimeseries=true&format=text";

    print("getting data available with $dataAvail ...\n");

    try {
        $stationData = file($dataAvail);
    }
    catch (Exception $e) {
        print("warning: could not get station data availability. skipping." . $e->getMessage() . "\n");
        continue;
    }

    // collect the stations from web service text output into an
    // array and sort by distance from event

    // #Network | Station | Latitude | Longitude | Elevation | SiteName | StartTime | EndTime 
    array_shift($stationData); // ditch header line shown above

    $n_sta = count($stationData);
    print("retrieved list of $n_sta stations, creating station objects ...\n");

    $stations = array();
    foreach($stationData as $sta) {
        $stations[] = new Station($sta, $evlat, $evlon);
        print_r($sta);
    } // end foreach stationData

    usort($stations, 'station_dist_cmp');

    // sanity check, make sure still same
    $n_sta = count($stations);

    if(false)  {
        dbgmsg("\nafter sorting $n_sta stations (as array):\n");
        foreach($stations as $s) {
           var_dump($s);
       }
    }

    //if($mode === 'rebuild' || $mode == 'repairID') {
        print("$mode mode: clearing out traces table and files cached for event " . $ev["evid"] . "...\n");
        $result = $file_db->query("DELETE FROM traces where evid = " . $ev["evid"]);
        $cols = $result->fetchAll (PDO::FETCH_BOTH); // for now assuming succeeded
        if(isSet($cols[0][0]))
            $trace_pre = intval($cols[0][0]); // may not exist if no traces deleted
        else
            $trace_pre = 0;
        dbgmsg("deleted existing traces for event");

        // delete all trace-related files
        // keep going even if fail, just issue warning
        
        $outpath = "data/" . $ev["evid"];

        $outary = array();
        $rc = 0;

        // delete jsonp files
        $rmfiles = "$outpath/*.jsonp"; 
        $cmd = "/bin/rm -rf $rmfiles";
        dbgmsg("executing $cmd");

        $lastline = exec($cmd, $outary, $rc);
        if($rc != 0 || $lastline != "") {
            print("warning: could not delete with $cmd \n");
            print_r($outary);
            unset($outary);
        }
        
        // delete raw trace images 
        $rmfiles = "$outpath/*.png"; 
        $cmd = "/bin/rm -rf $rmfiles";

        dbgmsg("executing $cmd");

        $lastline = exec($cmd, $evInfo, $rc);
        if($rc != 0 || $lastline != "") {
            print("warning: could not delete with $cmd \n");
            print_r($outary);
        }
    //}
    //else {
        //dbgmsg("left alone existing traces for event");
    //}

    print("fetching timeseries data for $n_sta stations ...\n");

    foreach($stations as $station) { 
        $dataUrl = "http://ds.iris.edu/gsv/timeseriesAsJsonDev.php" .
                    '?sta=' .    $station->code .
                    '&net=' .    $station->net .
                    //'&evlat=' .  $evlat .
                    //'&evlon=' .  $evlon .
                    '&evdep=' .  $evdep .
                    '&evdist=' . $station->distDeg .
                    '&start='  . $start ;
                    '&minSNR=10'+   // could also use 20, see scorePlots.php, what min it is using
                    //'&callback=?';

        dbgmsg("trying $dataUrl ...");
        try {
            // warning: this produces JSONP, not valid JSON, so 
			// doesn't parse with json_decode()
            $stationDataAry = file($dataUrl);
        }
        catch (Exception $e) {
            print("warning: could not get station timeseries as JSON using $dataUrl  skipping." . $e->getMessage() . "\n");
            continue;
        }
        //print_r($stationDataAry);
        if($stationDataAry === FALSE || count($stationDataAry) < 20) {
            print("warning: could not get station timeseries as JSON string using $dataUrl  skipping. \n");
            continue;
        }

/*
        output is like:
        [0] => callback(
        [1] => /* output for https://ser.....
        [2] => {
        [3] => "snr": 0,
        [4] => "firstPhase": "P",
        [5] => "firstPhaseTimeIndex": 39,
        [6] => "snrPhase": "P",
        [7] => "snrPhaseTimeIndex": 39,
        [8] => "nsPeaks": 8,
        [9] => "data":
        [10] => [
        [11] => [1262558189011,0.56985385326085],
 */

//print_r(array_slice($stationDataAry,0,10));

        list($jnk, $snr) =      explode(' ', trim($stationDataAry[3]));
        list($jnk, $snrPhase) = explode(' ', trim($stationDataAry[6]));
        list($jnk, $snrPhaseTimeIndex) = explode(' ',
                                             trim($stationDataAry[5]));
        list($jnk, $nsPeaks) =  explode(' ', trim($stationDataAry[8]));

        print("station " . $station->net . ":" . $station->code . " at " .
            $station->distDeg . " degs, phase " . $snrPhase .
            " has snr: " . $snr . " at array index " . $snrPhaseTimeIndex . " and # smoothed peaks: ". $nsPeaks . "\n");

        // write stationDataAry[] to JSON file

        $outpath = "data/" . $ev["evid"];
        $jsnfile = "$station->distDeg.$station->net.$station->code.jsonp";
        $imgfile = "$station->distDeg.$station->net.$station->code.png";
        //if($mode === "rebuild") {
            if(file_exists($jsnfile)) unlink($jsnfile);
            if(file_exists($imgfile)) unlink($imgfile);
        //}

        if(! file_exists($outpath) || ! is_writable($outpath))
            if(! mkdir($outpath, true))  // recursive, true
                die("could not create $outpath");
        chmod("$outpath", 0755);

        if($mode === "rebuild" || !file_exists("$outpath/$jsnfile")) 
            if(! file_put_contents("$outpath/$jsnfile", $stationDataAry))
                die("could not write to $outpath/$jsnfile");

        $sps = 1;
        $secs = 2000;

        //if(!file_exists("$outpath/$imgfile")) {
        if(TRUE) {
            $plotUrl = "http://service.iris.edu/irisws/timeseries/1/query?" .
                "net=$station->net&sta=$station->code&cha=BHZ&loc=00&deci=$sps&" .
                "demean=true&output=plot" .
                "&starttime=${start}&duration=$secs" .
                "&height=150&width=800&antialiasing=true";

            dbgmsg( "\n\nfetching image; $plotUrl\n\n" );
            //$out = array();
            exec("/usr/bin/curl --silent --fail  '" . $plotUrl . "' " .  "-o $outpath/$imgfile");

            // don't worry if failed to get image, it's for dev mode debugging only
            }

        dbgmsg( "updating traces table" );

        // heuristically determine ranges for usable metrics:
        if($snr >= 20 && ($nsPeaks >= 10 && $nsPeaks <= 250))
            $usable = '1';
        else
            $usable = '0';

        $nSamps = $sps * $secs;
        $traceQuery = 'replace into traces values (' .
                   $ev["evid"]  . ", null, " .
                   "$usable," .
                   "0," .  // usableOveridden
                   "null, " .  // usableReason
                   "'$station->net', " .
                   "'$station->code', " .
                   "'BHZ'," .
                   "'00'," .
                   //"'" . ::sqlite_escape_string($station->place) . "', " .
                   $file_db->quote($station->place) . ", " .
                   "$station->lat, " .
                   "$station->lng, " .
                    lon180To360($station->lng) . ',' .
                   "$station->distKm, " .
                   "$station->distDeg, " .
                   "'$start', " .
                   "'$end', " .
                   "$nSamps," .  // #samps
                   "1, " .  // sps
                   "$snrPhase " .    // already has comma on it
                   "$snrPhaseTimeIndex  " .    // already has comma on it
                   "$snr " .    // already has comma on it
                   "$nsPeaks " .   // already has comma on it
                   "'$dataUrl', " .
                   "'$plotUrl', " .
                   $file_db->quote("$outpath/$jsnfile") . ');';

        try {
           dbgmsg("query:\n$traceQuery\n");
           $stmt = $file_db->prepare($traceQuery);
           // Execute statement
           $stmt->execute();
        }
        catch(PDOException $e) {
           // Print PDOException message
           echo "\nError: failed to execute query; skipping \n";
           echo($e->getMessage());
           //if(isSet($busyfile) && file_exists($busyfile))
                //unlink($busyfile);
           //die($e->getMessage());
           continue;
      }

        /*
       try {
           $result = $file_db->query("COMMIT transaction");
         }
       catch(PDOException $e) {
           // Print PDOException message
           echo "\nError: failed to commit transaction\n";
           echo($e->getMessage());
           //$file_db = null; // close db
           die($e->getMessage());
          }
         */

       // this busy file could be used to tell index.php (web server) that we are finished
       //if(isSet($busyfile) && file_exists($busyfile))
          //unlink($busyfile);

       $result = $file_db->query("SELECT count(*)  FROM traces where evid = " . $ev["evid"]);
       $cols = $result->fetchAll (PDO::FETCH_BOTH);
       $trace_post = intval($cols[0][0]);

    	if($mode === 'rebuild') 
       		print("trace count before update: $trace_pre ; after update: $trace_post \n");


    } // end each station

    print("end of this event\n");

   } // end foreach event

   $file_db = null; // close db


   $local_time = trim(`/bin/date`);
   print("getTraces.php finished at $local_time\n\n");


?>
