.show

PRAGMA journal_mode=OFF;
PRAGMA synchronous=OFF;
PRAGMA temp_store=MEMORY;

PRAGMA page_size=8192;
PRAGMA read_uncommitted=ON;
PRAGMA autocheckpoint=0;

--PRAGMA default_cache_size=8000;
PRAGMA default_cache_size=16000;
PRAGMA cache_size=16000;
PRAGMA ignore_check_constraints=ON;
PRAGMA mmap_size=16000;


DROP TABLE IF EXISTS events;
--DROP TABLE IF EXISTS evinfo;
DROP TABLE IF EXISTS traces;
DROP TABLE IF EXISTS plots;
--DROP TABLE IF EXISTS zipcodes;

--datatypes in sqllite not critical, they become one of about
--4 basic types, but I leave as is to show what I "expect"

CREATE TABLE `events` (
  `evid` integer PRIMARY KEY,
  `time` timestamp NOT NULL ,
  `lat` decimal NOT NULL,
  `lon` decimal NOT NULL,
  `lon360` decimal NOT NULL,
  `depth` decimal NOT NULL,
  `author` char(18) NOT NULL,
  `catalog` char(18) NOT NULL,
  `contrib` char(18) NOT NULL,
  `contribID` char(18) NOT NULL,
  `magType` char(6) NOT NULL,
  `mag` decimal(3,0) NOT NULL,
  `magAuthor` char(10) NOT NULL,
  `region` text NOT NULL
  --`needsplot` integer default 1 
  --`nPlots` integer default 0,
  --`nTraces` integer default 0,
  -- `updTime` timestamp
);

-- stuff that applies to all plots made of a given event
-- usable stuff for manual overrides by EPO staff?
-- scorePlots.php should set usable here as well as PLOTS table...
CREATE TABLE `evinfo` (
  `evid` integer PRIMARY KEY,
  `title` char(120) , -- special events may need a modified, better title, subtitle
  `subTitle` char(120),

  `imgURL` char(120) , -- TBD
  `infoURL` char(120) , -- TBD

  `usable` integer default 1,		-- is it usable?
  `usableOveridden` integer default 0, -- did a human override that?
  `usableReason` text,  -- if so, why? (eg. mega quake needs to stay put)

  `lastUpdate` timestamp  
);


-- traces recorded for a given evid above
-- at various stations, with quality of trace 
-- and distances from event

CREATE TABLE `traces` (
  `evid` integer NOT NULL,  -- many traces for each evid
  `trid` integer PRIMARY KEY AUTOINCREMENT,  -- trace_id

   -- is this trace elibible/usable in a recsec plot?
   -- determined after loading so can disable/enable individually
   -- see getTraces(), SNR and nsPeaks (#smoothed peaks) determine it:
  `usable` integer NOT NULL default 1,  -- is it usable?
  `usableOveridden` NOT NULL default 0,  -- did a human override that?
  `usableReason` text,					-- if so, why?

  `net` char(5) NOT NULL,
  `sta` char(5) NOT NULL,
  `cha` char(3) NOT NULL,
  `loc` char(2) ,
  `place` char(40) ,

  `lat` decimal NOT NULL,
  `lon` decimal NOT NULL,
  `lon360` decimal NOT NULL,

  `distKm` decimal NOT NULL,
  `distDeg` decimal NOT NULL,

   -- start always at event time
   -- end currently at evTime + 30mins 
  `starttime` timestamp NOT NULL ,
  `endtime` timestamp NOT NULL ,

   -- num samples and samples/sec

  `ns` integer NOT NULL ,
  `sps` integer NOT NULL ,

   -- the overall snr and noise for the whole trace
   -- may be a combination of measurement points,
   -- probabably P < 100 degrees, and PP beyond that

  `snrPhase` char(5) NOT NULL,
  `snrIndex` decimal NOT NULL ,
  `snr` decimal NOT NULL ,

   -- the number of peaks after smoothing trace
   -- helpful for wandering traces or bizarre ones (eg.square sin)
   -- ones like only one phase arrival and totally flat elsewhere
   -- or high general noise, or a square sin wave-like shape

   -- heuristically < 10 or > 150 = BAD: for current sps settings
  `nsPeaks` integer NOT NULL ,


  --`signal` decimal NOT NULL ,
  --`noise` decimal NOT NULL ,

  -- specific snrs at various places for reference; not used now
  --    `snr_p` decimal NOT NULL ,
  --    `snr_pp` decimal NOT NULL ,
  --    `snr_s` decimal NOT NULL ,
  --    `snr_h` decimal NOT NULL ,

   -- some travel time data for reference; not used now
  --    `p_secs` decimal NOT NULL ,
  --    `pp_secs` decimal NOT NULL ,
  --    `s_secs` decimal NOT NULL ,

  -- convenience. data URL to fetch timeseries, 
  -- leaving format= unspecified, so can reuse for various formats
  -- old: `url` char(120) 
  `dataUrl` char(120),  -- JSONP file getTimeseriesAsJson.php
  `plotUrl` char(120),  -- IRIS web service timeseries plot PNG file
  `cacheFile` char(120) -- name of timeseries JSON file on disk

);


-- event plots that have been made (record sections)
-- note: char(), varchar, and text are all TEXT in sqlite

CREATE TABLE `plots` (

  --`plotid` integer PRIMARY KEY AUTOINCREMENT,  -- plot id, no need?
  `evid` integer NOT NULL ,  
  `nTraces` integer NOT NULL ,  -- (eg. 12,15,18 but if large it means all avail)

  `usable` integer NOT NULL default 0,
  `usableOveridden` integer default 0,
  `usableReason` TEXT default null,

   ------- when retrieving, split these into arrays, one value per trace
  `traceIds` TEXT default null,  -- comma-sep list of trace ids
  `SNRs` TEXT default null,  -- comma-sep list of SNRs
  `distsDegs` TEXT default null,  -- comma-sep list of distances
  `gapsDegs` TEXT default null,  -- comma-sep list of dists to next trace
   -------

  `degSpan` decimal default null,  -- 150 at least
  `minDist` decimal default null,  
  `maxDist` decimal default null,  
  `gapDegsAvg` decimal default null,  -- avg spacing
  `gapDegsStdev` decimal default null ,  
  `SNRsStdev` decimal default null ,  
  `SNRsAvg` decimal default null ,  
  `SNRsMedian` decimal default null ,  

  --`smallestGap` decimal default 0,
  --`widestGap` decimal default 0 ,
  --`density` TEXT default null,  --  'sparse', 'normal' or 'dense'

  `status` TEXT default null,  -- tbd
  `score` decimal default null,  -- tbd

  `updTime` timestamp default null,

  `url` TEXT default null,

   PRIMARY KEY('evid', 'nTraces')
);


-- another way other than using traceIdsList above
--- CREATE TABLE `plotTraceGaps` (
  --- `plotid` integer ,  -- is this key?
  --- `gapDegs` integer , -- 5, 10, or 15
  --- `traceid` integer   -- repeats once for each trace in gap of say 5, 10 etc.
--- );


















.schema

.separator "|"

.import allevents.nodupes.txt events

-- delete very tiny events (mainly Japan) or were null and flagged in weedDups.php

DELETE FROM events where mag <= 0;

-- delete events that had no depth and were flagged in weedDups.php

DELETE FROM events where depth == -999;


-- UPDATE events set updTime = datetime("now");



DROP INDEX IF EXISTS evidx;
CREATE UNIQUE INDEX evidx on events(evid);   

----------

DROP INDEX IF EXISTS all_idx;
CREATE INDEX all_idx on events (time, lat, lon, depth, mag);

DROP INDEX IF EXISTS all2_idx;
CREATE INDEX all2_idx on events (time, lat, lon360, depth, mag);

----------

DROP INDEX IF EXISTS time_idx;
CREATE INDEX time_idx on events (time);

DROP INDEX IF EXISTS time_asc_idx;
CREATE INDEX time_asc_idx on events (time asc);

DROP INDEX IF EXISTS time_desc_idx;
CREATE INDEX time_desc_idx on events (time desc);

----------

DROP INDEX IF EXISTS mag_idx;
CREATE INDEX mag_idx on events (mag);

DROP INDEX IF EXISTS mag_asc_idx;
CREATE INDEX mag_asc_idx on events (mag asc);

DROP INDEX IF EXISTS mag_desc_idx;
CREATE INDEX mag_desc_idx on events (mag desc);

----------

DROP INDEX IF EXISTS time_loc_idx;
CREATE INDEX time_loc_idx on events (time,lat,lon);

DROP INDEX IF EXISTS time_loc2_idx;
CREATE INDEX time_loc2_idx on events (time,lat,lon360);

DROP INDEX IF EXISTS loc_idx;
CREATE INDEX loc_idx on events (lat,lon);

DROP INDEX IF EXISTS loc2_idx;
CREATE INDEX loc2_idx on events (lat,lon360);

----------

DROP INDEX IF EXISTS lon_idx;
CREATE INDEX lon_idx on events (lon);

DROP INDEX IF EXISTS lon2_idx;
CREATE INDEX lon2_idx on events (lon360);

DROP INDEX IF EXISTS lat_idx;
CREATE INDEX lat_idx on events (lat);

----------

DROP INDEX IF EXISTS depth_idx;
CREATE INDEX depth_idx on events(depth);

DROP INDEX IF EXISTS depth_asc_idx;
CREATE INDEX depth_asc_idx on events(depth asc);

DROP INDEX IF EXISTS depth_desc_idx;
CREATE INDEX depth_desc_idx on events(depth desc);

DROP INDEX IF EXISTS mag_depth_idx;
CREATE INDEX mag_depth_idx on events (mag,depth);

DROP INDEX IF EXISTS time_depth_idx ;
CREATE INDEX time_depth_idx on events (time,depth);

DROP INDEX IF EXISTS time_mag_idx ;
CREATE INDEX time_mag_idx on events (time,mag);

--CREATE TABLE IF NOT EXISTS `zipcodes` (
    --`zipcode` integer NOT NULL,
    --`lat` decimal NOT NULL,
    --`lon` decimal NOT NULL,
    --`comment` text
--);

---- create a table for crude records/statistics on update
---- to be updated by either check_db.ksh or update_db.php
---- one purpose: updating fails if db is locked, so can detect and fix
---- db lock condition via cron job
---- NOTE: NOT USED NOW
CREATE TABLE IF NOT EXISTS `updates` (
    `updid` integer PRIMARY KEY AUTOINCREMENT,  -- plot id
    `time` timestamp NOT NULL ,
    `comment` text
);

INSERT into updates values(null,date('now'),'all tables but evinfo dropped, and events reloaded, no further entries currently ever made to this table');

--- fix the 2004 Asian tsunami great quake to have proper mag:
--- select * from events where evid=1916079;
--- 1916079|2004-12-26T00:58:52|3.4125|95.9012|95.9012|26.1|ISC|ISC|ISC|7900012|MW|8.2|NEIC|OFF W COAST OF NORTHERN SUMATERA
update events set mag=9.1 where evid=1916079;



.quit
