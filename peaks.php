﻿<?php

error_reporting(0);

function getExtrema($points) {

    $extrema = array();
    $last = null;
    $num = count($points);
    for($i=0;$i<$num - 1;$i++) {
        $curr = $points[$i];
        if($last === null) {
            $extrema[] = $curr;
            $last = $curr;
            continue;
        }

        //min
        if($last > $curr && $curr < $points[$i + 1]) {
            $extrema[] = $curr;
        }
        //maxes
        else if ($last < $curr && $curr > $points[$i + 1]) {
            $extrema[] = $curr;
        }
        if($last != $curr && $curr != $points[$i + 1]) {
            $last = $curr;
        }
    }
    //add last point
    //$extrema[] = $points[$num - 1];
    while($extrema[0] === NULL)
        array_shift($extrema);
    array_shift($extrema);

    return $extrema;
}

/* various tried smoothing functions, not very satisfactory:
function smooth($points) {
    $refined = [];

    foreach($points as $index => $point) {
      $prev = isset($points[$index - 1]) ? $points[$index - 1] : false;
      $next = isset($points[$index + 1]) ? $points[$index + 1] : false;

      if($point > 0 || ($prev && $prev > 0) || ($next && $next > 0)) {
      _  $refined[] = $point;

        while($next && $point < $next) {
          $point++;
          $refined[] = $point;
        }
      } else {
        $refined[] = $point;
      }
    }
    return $refined;
}

function array_bucket($array, $bucket_size)  // bucket filter
{
    $new_array = [];
    if (!is_array($array)) 
        return false; // no empty arrays
    $buckets=array_chunk($array, $bucket_size);  // chop up array into bucket size units
    foreach ($buckets as $bucket) 
        //$new_array[key($buckets)] = array_sum($bucket) / count($bucket);
        $new_array[] = array_sum($bucket) / count($bucket);
    return $new_array;  // return new smooth array
}


ended up with this one:
*/
function avg ($v) {
  return array_sum($v) / count($v);
}

function smoothAry ($vector, $variance) {  // afraid I don't know what variance does...
  $t_avg = avg($vector) * $variance;
  //echo "$t_avg\n";
  $ret = array();
  $vecCnt = count($vector);
  for ($i = 0; $i < $vecCnt; $i++) {
      $prev = $i > 0 ? $ret[$i - 1] : $vector[$i];
      $next = $i < $vecCnt  ? $vector[$i] : $vector[$i - 1];
      //echo "$prev  $next\n";
      $ret[$i] = round(avg(array($t_avg, $prev, $vector[$i], $next)), 5);
  }
  return $ret;
}

function smooth(array $data, $window) {
    $sum = array_sum(array_slice($data, 0, $window));

    $result = array($window - 1 => $sum / $window);

    for ($i = $window, $n = count($data); $i != $n; ++$i) {
        // try rounding it as too many peaks are found if data are in high precision:
        $result[$i] = round(($result[$i - 1] + ($data[$i] - $data[$i - $window]) / $window), 2);
        // orig w/o round
        //$result[$i] = ($result[$i - 1] + ($data[$i] - $data[$i - $window]) / $window);
    }

    return $result;
}


/* for testing: 

$points = array(0.2, 2.5, 5.99, 2.03, -3.4, -5.8, -6.5, -2.0, -6.0, -4.0, -2.0, 3.0, 8.0);
$extrema = getExtrema($points);
//$smoothed = smooth($points);
//$smoothed = array_bucket($points,2);
$smoothed = smooth($points, 2);

//print_r($points);
print_r($smoothed);
for($p = 0; $p < count($points); $p++)
    echo "$p:  p=$points[$p]  sm=$smoothed[$p]\n";



$extrema = getExtrema($smoothed);
print_r($extrema);
*/
?>
