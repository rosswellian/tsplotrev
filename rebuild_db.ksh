#!/bin/ksh

# rebuild plotsdb from scratch by fetching all events from web services and loading into fresh
# sqlite table; then copying that table to staging area where index.php looks (first) for it


echo $0 running at `date`

####### some vars   (more below)

workdir=/opt/seismon/gsv

# ultimate desired db:
db=plotsdb

# during build we operate on a copy, a temp version.
# it can be used to restore db if job fails.
tempdb=plotsdb_tmp
bkpdir=data

# url will fetch to:
wsdestfile=allevents.txt

# tail will strip column header line to:
tailtempfile=allevents.tmp

# weedDups.php creates this file if successful
nodupesfile=allevents.nodupes.txt


###### make sure needed files are present

if [[ "$workdir" != "$PWD" && "${workdir}/" != "${PWD}"   ]] ; then
   echo Error\: this script is intended to be run by seismon in $workdir
   exit -1
fi

if [[ ! -e load_db.sql ]] ; then
  echo Error\: could not find load_db.sql 
  exit -1
fi

if [[ ! -e weedDups.php ]] ; then
  echo Error\: could not find weedDups.php 
  exit -1
fi

#################################
#startdate=2008-01-01
startdate=2010-01-01
minmag=7.0
#################################

url="http://service.iris.edu/fdsnws/event/1/query?starttime=${startdate}&minmag=${minmag}&orderby=time&format=text&nodata=404" 

echo
echo "*** " warning\: this will delete and
echo repopulate the EVENTS table, and it currently 
echo also deletes the entire TRACES and PLOTS tables.
echo
echo used mainly during development not regular use
echo good for if schema change or min mag changes or filtering is added or time range change
echo
echo "DO NOT DO THIS ON A WHIM,  more likely use"
echo "update_db.php, creates new EVENTS in db, allows a recent time range like 30 days"
echo "then getTraces.php refresh" 
echo "-------------------------------------------------------"
echo
echo "the plan is:"
echo
echo get events from web service with
echo "   /usr/bin/wget $url -O $wsdestfile"
echo
echo zero out negative or missing mags and flag missing depths\:
echo get rid of the few duplicate event IDs that exist\, with
echo "   ./weedDups.php  "
echo
echo weedDups.php creates $nodupesfile
echo
echo "then CREATE TEMP DB, loading $nodupesfile, as $tempdb with:"
echo "   ~russ/sqlite/bin/sqlite -init load_db.sql $tempdb </dev/null"
echo
echo "then manually:"
echo "      php getTraces.php rebuild"
echo "then  mv $tempdb $db"
echo "and   php scorePlots.php "
echo
echo
echo enter c to continue q to quit
read go
if [[ "$go" != "c" ]] ; then
   echo "**" run cancelled "**" 
   exit
fi


##############################
# go get WS file
##############################

# orderby choices are:  time (same as time-desc) or time-asc

echo getting $url 

echo /usr/bin/wget $url -O $wsdestfile
/usr/bin/wget $url -O $wsdestfile

rc=$?

if [[ "$rc" != "0" ]] ; then
  echo Error\: problem with wget
  exit -1
fi


# not needed:   cat allevents.txt | sed -e 's/|/\t/g' > allevents.tabs

if [[ -e $nodupesfile && ! -w $nodupesfile ]] ; then
   echo "Error\: $nodupesfile  exists but is not writable, so weedDups.php will fail"
   exit -1
fi

echo weedDups\.php will now create $nodupesfile 
################  
./weedDups.php  
################  

rc=$?
if [[ "$rc" != "0" ]] ; then
  echo Error\: problem with weedDups.php
  exit
fi

nlines=`cat $nodupesfile | wc -l`

echo $nlines lines in nodupes text file 

# ditch col header line

(( nlines = nlines - 1 ))

echo ditching col header line\, so we want the tail $nlines lines

/bin/rm -rf $tailtempfile
if [[ -e $tailtempfile ]] ; then
  echo Error\: problem deleting $tailtempfile 
  exit -1
fi

eval cat $nodupesfile | tail -$nlines  >$tailtempfile

if [[ ! -e $tailtempfile ]] ; then
  echo Error\: could not find $tailtempfile after ditching header with tail
  exit -1
fi

mv $tailtempfile $nodupesfile
rc=$?
if [[ "$rc" != "0" ]] ; then
  echo Error\: could not rename $tailtempfile to $nodupesfile
  exit -1
fi

# add a needs_plot value of 1 (true) and null timestamp (ev_updated) to end of each line
#             typeset TMP_FILE=$( mktemp )
#             cp -p $nodupesfile "${TMP_FILE}"
#             sed -e 's/$/|1|/' "${TMP_FILE}" > $nodupesfile 

# load into sqlite

echo loading the events table and adding lon360 column

if [[ -e $tempdb ]] ; then
   /bin/rm -rf $tempdb
   rc=$?
   if [[ "$rc" != "0" ]] ; then
     echo Error\: problem deleting existing temp db file $tempdb
     exit -1
   fi
fi

# my own sqlite is a bit newer and has GNU readline support:

# $ /usr/bin/sqlite3 -version        
# 3.6.20
# $ ~russ/sqlite/bin/sqlite3 -version
# 3.7.17 2013-05-20 00:56:22 118a3b35693b134d56ebd780123b7fd6f1497668

# load_db.sql contains the table definitions and indexes and such.
# make sure have ~/.inputrc as well as FCEDITOR set, to get the
# vi command line editing, very convenient
#################################################################
/opt/seismon/russ/sqlite/bin//sqlite3 -init load_db.sql $tempdb </dev/null
#################################################################

if [[ ! -e $tempdb ]] ; then
  echo "Error\: problem creating $tempdb, load_db.sql failed?"
  exit -1
fi




echo
echo
echo "next part is too risky to automate at this time, therefore"
echo 
echo "do:  nohup php getTraces.php rebuild >nohup.out 2>&1"
echo
echo "which can take hours to run. rebuild mode operates on $tempdb"
echo "when it finishes and if is successful:"
echo
echo "then: cp $tempdb $db"
echo "and:  php scorePlots.php"


echo $0 finished successfully at `date`
exit  # leave this here it is intentional

# below are old remnants, this is done manually if getTraces runs OK and $tempdb is ready






cp $tempdb $db
rc=$?
if [[ "$rc" != "0" ]] ; then
   echo Warning\: problem copying $tempdb to $db will try again in 5 secs
   sleep 5
   cp $tempdb $db
   rc=$?
   if [[ "$rc" != "0" ]] ; then
       echo Error\: Could not copy $tempdb to $db
       #echo will still try to copy to ${bkpdir}
   else
       echo successfully copied
   fi
fi


echo a couple of tests\; are files same size\?

ls -l1 $db | read mod n who grp size mon day time file
#ls -l1 ${bkpdir}/${db} | read mod n who grp newsize mon day time file
ls -l1 ${tempdb} | read mod n who grp newsize mon day time file

echo old and new sizes \: $size vs $newsize

if [[ "$size" != "$newsize" ]] ; then
   echo Warning\: problem copying $tempdb to $db
   #echo Warning\: problem copying $tempdb to ${bkpdir}/${db}
   echo sizes do not match\: $size vs $newsize
   exit -1
fi

echo
echo $0 finished successfully at `date`
