<?php


class Remapper {

   // set once when object created
   private $oldmin; // minimum value in old dataset
   private $oldmax; // maximum value in old dataset
   private $newmin; // new lowest remapped output number
   private $newmax; // new biggest 
   private $slope;

  public function Remapper($oldmin, $oldmax, $newmin, $newmax) {
      $this->oldmin = floatval($oldmin); 
      $this->oldmax = floatval($oldmax);
      $this->newmin = floatval($newmin);
      $this->newmax = floatval($newmax);
      $this->slope = ($newmax - $newmin) / ($oldmax - $oldmin);
  }
   
  public function remap($value ) {
	 return $this->newmin + $this->slope * ($value - $this->oldmin);
  }
}

// USAGE/TESTING:  comment this out when not testing:
/*
$oldvals = array(-10,0,5,10,50);
$newmin = -100.;
$newmax =  100.;

$r = new Remapper(min($oldvals), max($oldvals), $newmin, $newmax);

print_r($oldvals);
foreach($oldvals as $val) {
   print("$val remapped ($newmin : $newmax) = " . $r->remap($val) . "\n"); 
}
*/
?>
