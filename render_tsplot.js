// Render Multiple URLs to file

"use strict";
var RenderUrlsToFile, arrayOfUrls, system;

system = require("system");

function waitFor(testFx, onReady, timeOutMillis) {
    var maxtimeOutMillis = timeOutMillis ? timeOutMillis : 3000, //< Default Max Timout is 3s
        start = new Date().getTime(),
        condition = false,
        interval = setInterval(function() {
            if ( (new Date().getTime() - start < maxtimeOutMillis) && !condition ) {
                // If not time-out yet and condition not yet fulfilled
                condition = (typeof(testFx) === "string" ? eval(testFx) : testFx()); //< defensive code
            } else {
                if(!condition) {
                    // If condition still not fulfilled (timeout but condition is 'false')
                    console.log("'waitFor()' timeout");
                    phantom.exit(1);
                } else {
                    // Condition fulfilled (timeout and/or condition is 'true')
                    console.log("'waitFor()' finished in " + (new Date().getTime() - start) + "ms.");
                    typeof(onReady) === "string" ? eval(onReady) : onReady(); //< Do what it's supposed to do once the condition is fulfilled
                    clearInterval(interval); //< Stop this interval
                }
            }
        }, 250); //< repeat check every 250ms
};


/*
Render given urls
@param array of URLs to render
@param callbackPerUrl Function called after finishing each URL, including the last URL
@param callbackFinal Function called after finishing everything
*/
RenderUrlsToFile = function(urls, callbackPerUrl, callbackFinal) {
    var getFilename, next, page, retrieve, urlIndex, webpage;
    urlIndex = 0;
    webpage = require("webpage");
    page = null;
    getFilename = function() {
        return "tsplot.pdf";  // or pdf, etc
    };
    next = function(status, url, file) {
        page.close();
        callbackPerUrl(status, url, file);
        return retrieve();
    };
    retrieve = function() {
        var url;
        if (urls.length > 0) {
            url = urls.shift();
            urlIndex++;
            page = webpage.create();

            /* works
            page.viewportSize = {
                width: 1400,
                height: 1900
            };
            page.paperSize = {
                width: '1450px',
                height: '1950px',
                margin: '20px'
            };*/
            page.viewportSize = {
                width: 2000,
                height: 2000
            };
            page.paperSize = {
                width: '2050px',
                height: '2050px',
                margin: '20px'
            };



            /*
            page.paperSize = {
                format: 'Tabloid',
                orientation: 'portrait',
                margin: '1cm'
            };*/

            page.settings.userAgent = "Phantom.js bot";
            return page.open("http://" + url, function(status) {
                var file;
                file = getFilename();
                if (status === "success") {


        // Wait for 'signin-dropdown' to be visible
/*
        waitFor(function() {
console.log('waiting for container to be hidden...');
            // Check in the page if a specific element is now visible
            return page.evaluate(function() {
                return $("#container").is(":hidden");
            });
        }, function() {
           console.log("The chart should be visible now.");
           //phantom.exit();
        });
        */



                    return window.setTimeout((function() {
                        page.evaluateJavaScript('function () {togglePhases();}');
                        page.evaluateJavaScript('function() { $("#controls").hide(); }');
                        page.render(file);
                        return next(status, url, file);
                    }), 20000);
                } else {
                    return next(status, url, file);
                }
            });
        } else {
            return callbackFinal();
        }
    };
    return retrieve();
};

arrayOfUrls = null;

if (system.args.length > 1) {
    arrayOfUrls = Array.prototype.slice.call(system.args, 1);
} else {
    arrayOfUrls = ["ds.iris.edu/gsv/tsplotForID.phtml?latest=1&epo=0"];
}

RenderUrlsToFile(arrayOfUrls, (function(status, url, file) {
    if (status !== "success") {
        return console.log("Unable to render '" + url + "'");
    } else {
        return console.log("Rendered '" + url + "' at '" + file + "'");
    }
}), function() {
    return phantom.exit();
});
