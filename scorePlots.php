#!/usr/bin/php
<?php

require_once "station.php";
require_once "getDistance.php";
require_once "longitude.php";
//require_once "combinations.php";
require_once "bestPicks.php";

require_once "dbgmsg.php";

//global $dbg; // is bein reset in closestStation????
$dbg = true; // override setting from dbgmsg.php

//error_reporting(E_ALL ^ E_NOTICE);
error_reporting(E_ALL );
date_default_timezone_set("UTC");

$local_time = trim(`/bin/date`);
print("$argv[0] running at $local_time ");


function array_median($arr) {
    $count = count($arr); //total numbers in array
    $middleval = floor(($count-1)/2); // find the middle value, or the lowest middle value
    if($count % 2) { // odd number, middle is the median
        $median = $arr[$middleval];
    } else { // even number, calculate avg of 2 medians
        $low = $arr[$middleval];
        $high = $arr[$middleval+1];
        $median = (($low+$high)/2);
    }
    return $median;
}

// see: http://www.mathsisfun.com/data/standard-deviation.html
//
// The standard deviation is a statistic that tells you how tightly all the various examples are
// clustered around the mean (average) in a set of data. When the examples are pretty tightly
// bunched together and the bell-shaped curve is steep, the standard deviation is small.
// When the examples are spread apart and the bell curve is relatively flat,
// that tells you you have a relatively large standard deviation.

//
if (!function_exists('stats_standard_deviation')) {
    /**
     * This user-land implementation follows the implementation quite strictly;
     * it does not attempt to improve the code or algorithm in any way. It will
     * raise a warning if you have fewer than 2 values in your array, just like
     * the extension does (although as an E_USER_WARNING, not E_WARNING).
     *
     * @param array $a
     * @param bool $sample [optional] Defaults to false
     * @return float|bool The standard deviation or false on error.
     */
    function stats_standard_deviation(array $a, $sample = false) {
        $n = count($a);
        if ($n === 0) {
            trigger_error("The array has zero elements", E_USER_WARNING);
            return false;
        }
        if ($sample && $n === 1) {
            trigger_error("The array has only 1 element", E_USER_WARNING);
            return false;
        }
        $mean = array_sum($a) / $n;
        $carry = 0.0;
        foreach ($a as $val) {
            $d = ((double) $val) - $mean;
            $carry += $d * $d;
        };
        if ($sample) {
           --$n;
        }
        return sqrt($carry / $n);
    }
}


print("opening database... ");

///////////////////////////// connect to database

$db_file = "plotsdb";
$db_dir = "/opt/seismon/gsv";
$db_dsn =  "sqlite:$db_file";

try {
      /*************************************
      * open connection                    *
      *************************************/
      echo "opening $db_dsn...\n";
      $file_db = new PDO($db_dsn);
      $file_db->setAttribute(PDO::ATTR_TIMEOUT, 5);  // 5 seconds
}
catch(PDOException $e) {
        // Print PDOException message
        echo "\nError: failed to open connection to $db_file\n";
        die($e->getMessage());
}

$file_db->setAttribute(PDO::ATTR_ERRMODE,
                       PDO::ERRMODE_EXCEPTION);
$file_db->setAttribute(PDO::ATTR_TIMEOUT, 12);  // 12 seconds

if($dbg) {
       try {
          $result = $file_db->query("PRAGMA table_info(events)");
       }
       catch(PDOException $e) {
          echo "\nError: failed to query $db_file \n";
          die($e->getMessage());
       }
       //var_dump($result);
       $cols = $result->fetchAll (PDO::FETCH_BOTH);
       //var_dump($cols);

       $num = count ($cols);
       echo "Sanity: number of columns in EVENTS table = " . $num . "\n\n";
       for ($i=0; $i<$num; $i++) {
          $numflds = max(array_keys($cols[$i]));
          for ($j=0; $j<$numflds; $j++) {
             echo $cols[$i][$j] . " ";
             }
          echo "\n";
       }
}

dbgmsg("WARNING: plots table will be recreated. check plots_bkp for backup");

try {
    $result = $file_db->query("drop table plots_bkp");
}
catch(PDOException $e) {
     print("warning: could not drop table plots_bkp before run " .  $e->getMessage());
}
try {
    $result = $file_db->query("create table plots_bkp as select * from plots");
}
catch(PDOException $e) {
     die("error: could not backup plots table to plots_bkp " .  $e->getMessage());
}
/*  NO NEED to delete all entries IF doing INSERT or REPLACE later,
 *  new, dual PRIMARY key columns of (evid, ntraces) makes possible,
 *  along with eliminations of plotid autoincrement (not needed)
try {
    $result = $file_db->query("delete from plots");
}
catch(PDOException $e) {
     die("error: could not delete all from plots " .  $e->getMessage());
}
*/


//$query = "select evid, time, mag, region from events order by evid asc  limit 10 ";
$query = "select evid, time, mag, region from events order by evid asc  ";
try {
    $result = $file_db->query($query);
}
catch(PDOException $e) {
     die("error: failed with $query on $db_file " .  $e->getMessage());
}

// fetch all events into one array
try {
    $events = $result->fetchAll (PDO::FETCH_BOTH); // BOTH means numeric and hash array indexes BOTH are there!
    //var_dump($events);
}
catch(PDOException $e) {
    print("warning: could not fetchAll results on $query " . $e->getMessage() . " skipping");
    continue;
}

$evcnt = 0;

foreach ($events as $ev ) {
    $evid = $ev["evid"];
    $evtime = $ev["time"];
    $evmag = $ev["mag"];
    $evregion = $ev["region"];
    dbgmsg("scoring event ID $evid , M$evmag @ $evtime  in $evregion\n");

    //var_dump($ev);


 	// order by dist ascending is important to logic
    $query = "select * from traces where evid = $evid and usable = 1 order by  distDeg asc  ";
    try {
        $result = $file_db->query($query);
    }
    catch(PDOException $e) {
         die("error: failed with $query on $db_file " .  $e->getMessage());
    }

    // fetch all events into one array
    try {
        $traces = $result->fetchAll (PDO::FETCH_BOTH); // BOTH means numeric and hash array indexes BOTH are there!
        //var_dump($events);
    }
    catch(PDOException $e) {
        print("warning: could not fetchAll results on $query " . $e->getMessage() . " skipping");
        continue;
    }
    $ntraces = count($traces);

    dbgmsg("found $ntraces usable traces for event ID $evid \n");

    if($ntraces == 0) {
        dbgmsg("deleting stray plots for event ID $evid ");
        $query = "delete from plots where evid = $evid  ";
        try {
            $result = $file_db->query($query);
        }
        catch(PDOException $e) {
             die("error: failed with $query on $db_file " .  $e->getMessage());
        }
    }
    else {
        dbgmsg( "updating plots table for found traces" );

        $traceIds = array();
        $SNRs = array();
        $distsDegs = array();
        $gapsDegs = array(); // gaps to next trace

        $minDist = 999;
        $secondMinDist = 999; // second most min
        $maxDist = -1;
        $maxGap = -1;

        $minSNR = 999.;
        $maxSNR = -1.;

        $prevDist = -1.;
        //$nsPeaks = array();

        foreach ($traces as $tr ) {  // $traces may be empty!
            $trid = $tr["trid"]; // integer, so string OK
            $SNR = floatval($tr["snr"]);
            $dist = floatval($tr["distDeg"]);
            array_push($traceIds, $trid);
            array_push($SNRs, $SNR);
            array_push($distsDegs, $dist);
            if($minDist == 999) // this will be true on first dist encountered
                $minDist = $dist;
			elseif($secondMinDist == 999) // this will be true on second dist encountered
                $secondMinDist = $dist;

            if($dist > $maxDist) $maxDist = $dist;
            if($SNR < $minSNR) $minSNR = $SNR;
            if($SNR > $maxSNR) $maxSNR = $SNR;
            if($prevDist >=0)
                $thisGap = round($dist - $prevDist, 1);
            else
                $thisGap = round($dist, 1);;
            $prevDist = $dist;
            if($thisGap > $maxGap)
                $maxGap = $thisGap;

            //dbgmsg( "trace ID $trid at dist $dist is gap: $thisGap" );
            array_push($gapsDegs, $thisGap);
            //array_push($nsPeaks, floatval($tr["nsPeaks"]));
            //var_dump($tr);
            // get avg SNR etc and update plots table
        }
        //var_dump($SNRs);
        //
        $degSpan = $maxDist - $minDist;
        $medianSNR = array_median($SNRs);
        $stdevSNR = round(stats_standard_deviation($SNRs));
        $avgSNR = round(array_sum($SNRs) / count($SNRs));

        $gapsDegsAvg = round(array_sum($gapsDegs) / count($gapsDegs), 1);
        $gapsDegsStdev = round(stats_standard_deviation($gapsDegs), 1);

        dbgmsg( "min and max dists: $minDist, $maxDist , spanning $degSpan degrees " );
        dbgmsg( "traceIds:  " . join(",", $traceIds));
        dbgmsg( "distsDegs:  " . join(",", $distsDegs));
        dbgmsg( "gapsDegs:  " . join(",", $gapsDegs));
        dbgmsg( "gapsDegsAvg:  " . $gapsDegsAvg );
        dbgmsg( "gapsDegsStdev:  " . $gapsDegsStdev );
        dbgmsg( "SNRs:  " . join(",", $SNRs));
        dbgmsg( "min, max and avg SNRs: $minSNR, $maxSNR, $avgSNR " );
        dbgmsg( "avg SNRs:  " . $avgSNR );
        dbgmsg( "median SNRs: $medianSNR " );
        dbgmsg( "stdev of SNRs: $stdevSNR " );
        dbgmsg( "" );

        // attempt at super intelligent picking; abandoned for now
        //echo("calling  bestIndicesComboForGap\n");
        //bestIndicesComboForGap($traceIds , $distsDegs, $SNRs,  10);
        //                                                                 ask for 5 equally spaced traces
        //bestIndicesComboForGap(array_slice($traceIds,0,5) , array_slice($distsDegs,0,5) , array_slice($SNRs,0,5),  5);

        $usable = 1;
        $reason = "";
        if($ntraces < 8) {
            $usable = 0;
            $reason = "plot has too few usable traces (<8); aftershock?";
        }
        elseif($minDist > 18 || ($minDist < 3 && $secondMinDist > 18)) {
            $usable = 0;
            $reason = "plot has no usable nearby traces (2-18 deg)";
        }
        elseif($maxDist < 150) {
            $usable = 0;
            $reason = "plot has no usable distant traces (>=150 deg)";
        }
        elseif($medianSNR < 10) {
            $usable = 0;
            $reason = "plot has low median SNR value for traces (<10)";
        }
        elseif($maxGap > 20) {
            $usable = 0;
            $reason = "plot has a big trace gap (>20 deg)";
        }
        else {
            $shadowCnt = numDistsInZone(105, 140, $distsDegs);
            echo "**  evid $evid has $shadowCnt traces in shadow zone\n";
            if($shadowCnt == 0) {
                $usable = 0;
                $reason = "plot has no usable traces in shadow zone (105-140 deg)";
            }
        }

        dbgmsg( "event $evid,  max gap $maxGap,\tusable:$usable  (if not, reason: $reason) " );
        dbgmsg( "event $evid,  mag $evmag,\tusable:$usable  (if not, reason: $reason) " );


        // update PLOTS table, see load_db.sql for table & column definition

        //$plotQuery = "insert or replace into plots values (" .
        $plotQuery = "          replace into plots values (" .
        //$plotQuery = "insert into plots values (" .   // this is used if all values are deleted first, at start of script
                    //" null," .    // plotid
                    " $evid," .
                    " $ntraces," .  // all avail traces, later, below, we add 10,12,15,18, the equally-space plots

                    " $usable," .   // already has comma on it
                    " 0," .         // usableOverridden? might be done manually by human later
                    $file_db->quote($reason) . "," .   // usableReason column

                    " '" . join(",",$traceIds)  . "'," .    // already has comma on it
                    " '" . join(",",$SNRs)      . "'," .    // already has comma on it
                    " '" . join(",",$distsDegs)  . "'," .   // already has comma on it
                    " '" . join(",",$gapsDegs)  . "'," .    // already has comma on it

                    " $degSpan," .
                    " $minDist," .
                    " $maxDist," .
                    " $gapsDegsAvg," .
                    " $gapsDegsStdev," .
                    " $avgSNR," .
                    " $stdevSNR," .
                    " $medianSNR," .

                    " 'score not yet used so 0', " .
                    " 0, " .
                    " datetime('now'), " .
                    "'http://ds.iris.edu/gsv/tsplotBuildForID.phtml?evid=$evid' )";

       try {
            $result = $file_db->query("BEGIN transaction");
       }
       catch(PDOException $e) {
           // Print PDOException message
           echo "\nError: failed to begin a transaction\n";
           echo($e->getMessage());
           die($e->getMessage());
       }
       try {
           dbgmsg("query:\n$plotQuery\n");
           $stmt = $file_db->prepare($plotQuery);
           // Execute statement
           $stmt->execute();
       }
       catch(PDOException $e) {
           // Print PDOException message
           echo "\nError: failed to execute query; skipping \n";
           echo($e->getMessage());
           continue;
       }

       try {
           $result = $file_db->query("COMMIT transaction");
       }
       catch(PDOException $e) {
           // Print PDOException message
           echo "\nError: failed to commit transaction\n";
           echo($e->getMessage());
           die($e->getMessage());
       }


    if($usable == 1) { // only create usable plots

       // for each of several common plot densities add entries like above to PLOTS
       // these will be the key table entries for making well-spaced plots later

        //for($nPicks = 12; $nPicks <= 16; $nPicks += 2) { // later multiple "densities" of the plot
        for($nPicks = 8; $nPicks <= 36; $nPicks += 2) { // later multiple "densities" of the plot

            $picksIndices = bestDists($distsDegs, $SNRs, "npick", $nPicks ); // npick or gapdegs are possible

            print( "the best dists for picking $nPicks are: " . join(",", $distsDegs) . " \n");
            //foreach($picksIndices as $pick)
	            //echo "$distsDegs[$pick],";
            //echo "\n";
            //
            // use picksIndices as keys to choose corresponding values from original arrays:
            $picksDegs = array_map(function($key)     use ($distsDegs) { return  $distsDegs[$key]; },$picksIndices);
            //var_dump($picksDegs);
            $picksTraceIds = array_map(function($key) use ($traceIds) {  return  $traceIds[$key]; }, $picksIndices);
            $picksSNRs = array_map(function($key)     use ($SNRs) {      return  $SNRs[$key]; },     $picksIndices);

            $pickMin = min($picksDegs);
            $pickMax = max($picksDegs);
            $degSpan = $pickMax - $pickMin;

            $minSNR = min($picksSNRs);
            $maxSNR = min($picksSNRs);
            $avgSNR = array_sum($picksSNRs) / count($picksSNRs);
            $medianSNR = array_median($picksSNRs);
            $stdevSNR = round(stats_standard_deviation($picksSNRs));

            $gaps = array();
            //for($p=0; $p<$nPicks-1; $p++)
            for($p=0; $p<count($picksDegs)-1; $p++)
                array_push($gaps, ($picksDegs[$p+1] - $picksDegs[$p]));
            //var_dump($gaps);
            $gapsDegsAvg = round(array_sum($gaps) / count($gaps), 1);
            $gapsDegsStdev = round(stats_standard_deviation($gaps), 1);
            dbgmsg( "pick min and max dists: $pickMin, $pickMax , spanning $degSpan degrees " );
            dbgmsg( "pick traceIds:  " . join(",", $picksTraceIds));
            dbgmsg( "pick distsDegs:  " . join(",", $picksDegs));
            dbgmsg( "degSpan: $degSpan");
            dbgmsg( "gapsDegs:  " . join(",", $gaps));
            dbgmsg( "gapsDegsAvg:  " . $gapsDegsAvg );
            dbgmsg( "gapsDegsStdev:  " . $gapsDegsStdev );
            dbgmsg( "SNRs:  " . join(",", $picksSNRs));
            dbgmsg( "min, max and avg SNRs: $minSNR, $maxSNR, $avgSNR " );
            dbgmsg( "stdev SNRs:  " . $stdevSNR );
            dbgmsg( "median SNR: $medianSNR " );
            dbgmsg( "stdev of SNR: $stdevSNR " );
            dbgmsg( "" );


            $plotQuery = "replace into plots values (" .
            //$plotQuery = "insert or replace into plots values (" .
            //$plotQuery = "insert into plots values (" .   // this is used if all values are deleted first, at start of script
                    //" null," .    // plotid
                    " $evid," .
                    " $nPicks," .  // eg 10,12,15,18, the equally-space plots

                    " $usable," .   // already has comma on it
                    " 0," .         // usableOverridden? might be done manually by human later
                    $file_db->quote($reason) . "," .   // usableReason column

                    " '" . join(",",$picksTraceIds)  . "'," .    // already has comma on it
                    " '" . join(",",$picksSNRs)      . "'," .    // already has comma on it
                    " '" . join(",",$picksDegs)  . "'," .   // already has comma on it
                    " '" . join(",",$gaps)  . "'," .    // already has comma on it

                    " $degSpan," .
                    " $pickMin," .
                    " $pickMax," .
                    " $gapsDegsAvg," .
                    " $gapsDegsStdev," .
                    " $avgSNR," .
                    " $stdevSNR," .
                    " $medianSNR," .

                    " 'score not yet used so 0', " .
                    " 0, " .
                    " datetime('now'), " .
                    "'http://ds.iris.edu/gsv/tsplotForID.phtml?evid=$evid&npicks=$nPicks' )";
                    //"'http://ds.iris.edu/gsv/tsplotBuildForID.phtml?evid=$evid&npicks=$nPicks' )";

           try {
                $result = $file_db->query("BEGIN transaction");
           }
           catch(PDOException $e) {
               // Print PDOException message
               echo "\nError: failed to begin a transaction\n";
               echo($e->getMessage());
               die($e->getMessage());
           }
           try {
               dbgmsg("query:\n$plotQuery\n");
               $stmt = $file_db->prepare($plotQuery);
               // Execute statement
               $stmt->execute();
           }
           catch(PDOException $e) {
               // Print PDOException message
               echo "\nError: failed to execute query; skipping \n";
               echo($e->getMessage());
               continue;
           }

           try {
               $result = $file_db->query("COMMIT transaction");
           }
           catch(PDOException $e) {
               // Print PDOException message
               echo "\nError: failed to commit transaction\n";
               echo($e->getMessage());
               die($e->getMessage());
           }

        } // end for each of nPicks picks

    }   // end if usable

    }// end else ntraces >0


    print("end of this event\n");
    $evcnt ++;

   } // end foreach event

   $file_db = null; // close db


   $local_time = trim(`/bin/date`);
   print("$argv[0] finished at $local_time $evcnt events processed ");

   exit;




   function numDistsInZone($minDist, $maxDist, $distsAry) {
       $inCount = 0;
       foreach($distsAry as $dist)
           if($dist >= $minDist && $dist <= $maxDist)
               $inCount++;
       return $inCount;
   }


?>
