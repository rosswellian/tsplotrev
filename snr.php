﻿<?php

error_reporting(0);

function sqr($val) {
    return($val * $val);
}

function rms($ary) {
// root mean squared
// count the values
    $n = count($ary);
    if($n == 0) return(1);
    $squares = array_map('sqr', $ary);
    $avgSquare = array_sum($squares) / $n;
    return(sqrt($avgSquare));
}

function snr($signalAry, $noiseAry) {
    $sigAmplitude =   rms($signalAry);
    $noiseAmplitude = rms($noiseAry);
    $ampRatio = $sigAmplitude / ( $noiseAmplitude != 0 ? $noiseAmplitude : 1);
    //print("sigAmp: " . $sigAmplitude."\n");
    //print("noiseAmp: " . $noiseAmplitude."\n");
    //print("ratio: " . $ampRatio."\n");
    //print("squared: " . $ampRatio*$ampRatio."\n");
    return(pow($ampRatio,2));
}

/* for testing purposes

$ary=array(2.,-3.,2.,-3.);
$sig=array(2.,2,-10,-2,2);

//$r = range(mt_rand(0, 10), mt_rand(10, 20));
$r = range(-1., 2.,0.01);
$s = range(-1., 2.,0.01);
array_push($r,500);
array_push($s,500);
shuffle($r);
print_r($r);

print("rms: " . rms($ary)."\n");
print("snr: " . snr($sig, $ary)."\n");
print("snr r: " . snr($s, $r)."\n");

*/

?>
