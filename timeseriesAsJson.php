<?php

// emit a JSONP timeseries suitable for JavaScript caller to plot

include "remapper.php"; // for remapping counts to new value range

date_default_timezone_set("UTC");

// the (JSONP) JavaScript function that will be called by client
if(!isSet($_GET['callback']))
   $cb = 'callback';
else
   $cb = $_GET['callback'];


// start time in ISO (web services) fmt
if(!isSet($_GET['start']))
   $start = '2017-04-28T20:23:17'; // Mindanao Philippines
else
   $start = $_GET['start'];

// how many minutes of data, end time computed from start
if(!isSet($_GET['mins']))
   $mins = '30';
else
   $mins = $_GET['mins'];


if(!isSet($_GET['net']))
   $net = 'IU';
else
   $net = $_GET['net'];

if(!isSet($_GET['sta']))
   $sta = 'ANMO';
else
   $sta = $_GET['sta'];

if(!isSet($_GET['chan']))
   $chan = 'BHZ';
else
   $chan = $_GET['chan'];

if(!isSet($_GET['loc']))
   $loc = '00';
else
   $loc = $_GET['loc'];

if(!isSet($_GET['evlat']))
   $evlat = '00';
else
   $evlat = $_GET['evlat'];
if(!isSet($_GET['evlon']))
   $evlon = '00';
else
   $evlon = $_GET['evlon'];
if(!isSet($_GET['evdep']))
   $evdep = '10';
else
   $evdep = $_GET['evdep'];
if(!isSet($_GET['evdist']))
   $evdist = '120';
else
   $evdist = $_GET['evdist'];

// future use:
// minimum SNR to accept
if(!isSet($_GET['minSNR']))
   $minSNR = 0; // unspecified, don't care?
else
   $minSNR = $_GET['minSNR'];
// future use:
// whether to search for a cached timeseries or not? 
if(!isSet($_GET['usecache']))
   $usecache = false;
else
   $usecache = $_GET['usecache'];
// future use:
// whether to save to cache (filesystem and database) this timeseries
if(!isSet($_GET['save']))
   $save = false;
else
   $save = $_GET['save'];


if(!isSet($_GET['deci'])) { // aka sps or Hz
   //$sps = 0.25;
   //$sps = 0.5;
   //$sps = 4;  // too high; maybe ok if about <=8 traces
   $sps = 1;
   $deci = "&deci=$sps"; // the complete URL param
}
else {
   $sps = 0; // 0 means don't specify deci= in URL
   $deci = "";
}

$sps = floatval($sps);

if($sps !== 0 &&
   $sps !== 0.25 &&
   $sps !== 0.5 &&
   //$sps !== 0.75 && // not allowed
   $sps !== 1. &&
   $sps !== 2. &&
   $sps !== 4. ) {
  print("$cb(/* deci= must be 0.25, 0.5, 1, 2 or 4 - if passed */" . "[ ]);" );
  return;
}

// normalize Y vals to +/1 normrange/2
if(!isSet($_GET['normrange']))
    // normrange is "amplitude" in effect.
    // it how "wide/high" high spikes will be on recsec plot, in degs.
    // with denser (more traces) plots, then you'd lower this number.
    // logic for scaling values after drawn on plot exists in tsplotrev.html, as well.
    $normrange=25.; // bigger values than 1 cause some stations problems, e.g. RAR, SNZO
else
   $normrange = $_GET['normrange'];

if($normrange < 1. || $normrange > 100. ) {
  print("$cb(/* normrange must be between 1 and 100 if passed */" . "[ ]);" );
  return;
}

// half the new target range is positive, half negative:
$posrange = ($normrange > 0. ? floor($normrange / 2.) : 0.);
$negrange = - $posrange;

try {
    $startEpoch = strtotime($start); // will add time below
}
catch (Exception $e) {
    print("$cb(/* could not parse $start as valid date  */" . "[ ]);" );
    return;
}

$end = date('Y-m-d\TH:i:s', $startEpoch + floor($mins * 60)); // end in UNIX seconds

// ask for N seconds more up front than want, so can
// discard FFT-related spike at start of trace.
// this is far better than using a taper.
$fakeStart = date('Y-m-d\TH:i:s', $startEpoch - (30 * $sps));

//echo "start: $start , end: $end\n";
//exit;

// beware on tapering, it makes the trace fade out at both ends artificially
// at come decimations you get "wraparound" FFT artifact at start of trace (bad web services!)
// so if using tapering consider discarding the end of the trace (after asking for extra time at end)
$taper=0;  // test tapering 

$url = "https://service.iris.edu/irisws/timeseries/1/query?" .
        "net=${net}&sta=${sta}&cha=${chan}&loc=${loc}${deci}&" .
        "demean=true&output=ascii2&taper=${taper}" .
        "&starttime=${fakeStart}&endtime={$end}";

//echo "url: $url\n";
//exit;

/////////////////////////
// Turn off all error reporting
error_reporting(0);
try {
    $timeseries = file($url);
}
catch (Exception $e) {
  print("$cb(/* error getting timeseries: $url ," .  $e->getMessage() . "*/" .
        "[ ]);" );
  return;
}
error_reporting(E_ERROR | E_WARNING | E_PARSE);
/////////////////////////

if($timeseries === FALSE) {
  print("$cb(/* error getting timeseries for $url  */" .
        "[ ]);" );
  return;
}
// TIMESERIES 

if(!isSet($timeseries[0]) || strlen($timeseries[0]) < 50) {
  print("$cb(/* error getting timeseries: $url  */" .
        "[ ]);" );
  return;
}

// typical web service output:
// TIMESERIES IU_ANMO_00_BHZ_D, 7200 samples, 2 sps, 2010-02-27T06:30:00.019500, TSPAIR, FLOAT, COUNTS
// 2010-02-27T06:30:00.019500  -26567.449
// 2010-02-27T06:30:00.519500  -51493.125

// desire JSON format:
// callback(/* some comment or error info with URL used to attempt fetch */
// [
// [1268179200000,32.12],
// [1268265600000,32.21],
// [1268352000000,32.37],
// .....
// [1488844800000,139.52]
// ]);

// first get travel times so can measure SNR for the trace just fetched
$url = "https://service.iris.edu/irisws/traveltime/1/query?" .
        "distdeg=${evdist}&evdepth=${evdep}&phases=P,PP,PKIKP&mintimeonly=true";
/////////////////////////
// Turn off all error reporting
error_reporting(0);
try {
    $traveltimes = file($url);
}
catch (Exception $e) {
  print("$cb(/* error getting travel times: $url ," .  $e->getMessage() . "*/" .
        "[ ]);" );
  return;
}
error_reporting(E_ERROR | E_WARNING | E_PARSE);
/////////////////////////

if($traveltimes === FALSE) {
  print("$cb(/* error getting traveltimes: $url  */" .
        "[ ]);" );
  return;
}
if(!isSet($traveltimes[0]) || strlen($traveltimes[0]) < 8 ||
    strpos($traveltimes[0], "Model:") === FALSE || 
    count($traveltimes) < 5 ) {  // there are 4 header lines, see below
  print("$cb(/* error getting traveltimes: $url  */" .
        "[ ]);" );
  return;
}

// TODO
// if($evdist == -1)
//      we need to compute the distance by looking up the station's lat/lon


// Choose which phase at which to sample SNR
if($evdist >= 0. && $evdist <= 98.)
    $snrPhase = "P";
else
    if($evdist > 98. && $evdist <= 114.)
        $snrPhase = "PP";
    else
        if($evdist > 114. && $evdist <= 180. )
            $snrPhase = "PKIKP";
        else {
            print("$cb(/* invalid event to station distance: $evdist  */" .
                  "[ ]);" );
            return;
        }
/*
// typical web service output:
Model: iasp91
Distance   Depth   Phase   Travel    Ray Param  Takeoff  Incident  Purist    Purist
  (deg)     (km)   Name    Time (s)  p (s/deg)   (deg)    (deg)   Distance   Name
-----------------------------------------------------------------------------------
    2.00    26.0   P         32.08    13.753     53.83    45.84     2.00   = P
    2.00    26.0   PP        39.60    13.754     53.83    45.84     2.00   = PP
  ......
  114.00    26.0   PKIKP   1116.46     1.915      6.45     5.73   114.00   = PKIKP
  114.00    26.0   PP      1170.60     7.090     24.59    21.70   114.00   = PP

  Use this table to see Shadow Zone:
  https://service.iris.edu/irisws/traveltime/1/query?distdeg=2,10,20,32,50,55,90,95,96,97,98,99,100,101,102,103,104,105,107,110,111,112,113,114,115,120,125,130,138,144,152,158,171&evdepth=26&phases=P,PP,PKIKP&mintimeonly=true

  Our plan:
  from   to   find SNR at phase
  ----------------------------
   0    98    P
  99   113    PP
  114  180    PKIKP
*/

// we checked it has > 5 elements already above
//var_dump($traveltimes);
// we ask for 3 phases so output could contain all 3 but usually 1 or 2
$traveltimes = array_slice($traveltimes, 4);
//var_dump($traveltimes);

$snrSampleTimeSecs = -1; // flag val
foreach($traveltimes as $line) {
    list($jnk, $dist, $dep, $phase, $secs) = preg_split("/[\s,]+/", $line);
    //echo("dist: $dist, depth:$dep, phase:$phase, secs:$secs \n") ;
    if($phase == $snrPhase ) {
        //echo("got it \n") ;
        $snrSampleTimeSecs = $secs;
    }

}
//echo("snrSampleTimeSecs : $snrSampleTimeSecs \n") ;

if($snrSampleTimeSecs == -1) {
    print("$cb(/* failed to find travel time for $snrPhase using : $url  */" .
      "[ ]);" );
    return;
}

// now setup starting points in timeseries array for where SNR
// measurement will begin and for how many samples
//
// 30 samples before arrival time, +2 for fudge
$noiseIndex =   round($snrSampleTimeSecs * $sps - (32 * $sps));
$noiseLength =  round(25 * $sps);
// signal lasts 40 samples
$signalIndex =  round($snrSampleTimeSecs * $sps);
$signalLength = round(40 * $sps);  // for 40 secs

// at very very close stations, 0-2 degs, noise region may go negative and need clipping
if($noiseIndex < 0) {
    $noiseLength -=  noiseIndex;
    $noiseIndex =  0;
}


//echo("noise, start at element: $noiseIndex for: $noiseLength elements\n");
//echo("that is ".floor($noiseIndex/60.)." mins  : ". $noiseLength % 60 . " secs\n");
//echo("signal, start at element: $signalIndex for: $signalLength elements\n");
//echo("that is ".floor($signalIndex/60.)." mins  : ". $signalIndex % 60 . " secs\n");

//exit;


////////////////
// PROCESS timeseries
////////////////


array_shift($timeseries); // lose header line

$nlines = count($timeseries);
$xvals = array();
$stavals = array();

for($n=0; $n < $nlines; $n++) {
   $line = $timeseries[$n];
   $words = preg_split('/\s+/', $line);
   $ts = $words[0];
   $val = $words[1];

   try {
        $d = new DateTime($ts);  // ts = time string
   }
   catch (Exception $e) {
      // timeseries, if has a gap, will output a new header line, as in:
      // 2010-02-27T07:30:02.510600  450940.75
      // 2010-02-27T07:30:03.010600  232234.34
      // TIMESERIES IU_KMBO_00_BHZ_D, 651 samples, 2 sps, 2010-02-27T07:30:34.510600, TSPAIR, FLOAT, COUNTS
      // 2010-02-27T07:30:34.510600  -214537.28
      // 2010-02-27T07:30:36.010600  -482356.97

      //echo 'Caught exception: ',  $e->getMessage(), "\n";
      print("$cb(/* probable gap encountered in timeseries for $url  */" .  '[ ]);' );
      return;
   }

   $secs = intval($d->getTimestamp()); // get seconds since epoch

   // get ms portion, div by 1000 since web service considers the "." in string as a decimal point
   // if you DON'T do this you will have:
   //   secs: 1267252560  ms: 19500
   //   secs: 1267252560  ms: 519499
   //   secs: 1267252561  ms: 19500
   //   secs: 1267252561  ms: 519499

   $ms = (floatval($d->format('u')) / 1000.);
   // NOW you have:
   //   secs: 1267252560  ms: 19.5
   //   secs: 1267252560  ms: 519.499
   //   secs: 1267252561  ms: 19.5
   //   secs: 1267252561  ms: 519.499
   //   secs: 1267252562  ms: 19.5


   // drop unneeded precision, deps. on sps, this is for deci=2: want only 0 and 500 ms
   //$ms = ($ms < 500. ? 0 : 500); 

   // better to just round:
   $ms = round($ms);
   // NOW you have nice whole numbers:  (again for deci=2)
   //   secs: 1267252560  ms: 20
   //   secs: 1267252560  ms: 519
   //   secs: 1267252561  ms: 20
   //   secs: 1267252561  ms: 519

   //echo "secs: $secs  ms: $ms  val: $val\n";
   //if($n>5) exit;

   $epoch = $secs * 1000 + $ms; // highcharts wants milliseconds since epoch
   //$epoch = date("U", strtotime($ts)) * 1000.; // highcharts wants ms not secs

   // rcw TEST, artifacts at start due to decimation.
   // taper is one solution but tapers both ends, not jsut start
   // so discart the first sec or two (4 samples = 1sec)
   // $skipElems = floor(10. / $sps); // e.g. 5 secs at 1sps
//echo "skipping $skipElems\n";
//exit;
     if($secs >= $startEpoch) {
        $stavals[] = $val; // same as array_push
        $xvals[] = $epoch;
   }
}



$posrange = ($normrange > 0. ? floor($normrange / 2.) : 0.);
$negrange = - $posrange;

if($normrange > 0.) {

   //var_dump($stavals[$station]);
   //var_dump(array_map("floatval", $stavals[$station]));

   //rcw   print_r(array_slice($stavals, 0, 90)); 

   // convert to float so can remap to new range
   $stavals = array_map("floatval", $stavals);

   // you MUST not count the first two values 
   // in max and min, UNLESS you are using taper=true
   // as first two values are "bad".
   // as in max(array_slice($stavals, 2 )); 

   $maxseen = max(array_slice($stavals,2));
   $minseen = min(array_slice($stavals,2));
   //var_dump($minseen);
   //var_dump($maxseen);
   //var_dump($negrange);
   //var_dump($posrange);


   $r = new Remapper($minseen, $maxseen, $negrange, $posrange);
   //$r = new Remapper($minseen, $maxseen, 0., $normrange);


   //var_dump($r);
   //print($r->remap(100)."\n");

   $stavals = array_map(array($r, 'remap'), $stavals);
   //$stavals = array_map(function($v) { return round($v,4);},  $stavals);

   //print_r(array_slice($stavals, 0, 500)); 
   //exit;

   // rcw test: our trace should be flatlining more or less at start,
   // so sample and adjust the trace up or down if not.
   // problem is being off by 1 or 2, if drawn in a rec sec plot,
   // means it is not anchored properly at the distance it belongs at,
   // one that is a bit negative drawn just next to another that is
   // a bit positive yields two traces literally on top of each other,
   // which is not possible or correct.  see RAR and SNZO w/Chile test

   $avg = array_sum(array_slice($stavals,5,30)) / 30.;
   //print("avg: $avg\n");
   if(abs($avg) > 1.)
       for($s = 0; $s < count($stavals); $s++)
           $stavals[$s] -= $avg;
   else
       for($s = 0; $s < count($stavals); $s++)
           $stavals[$s] -= $avg;



   // convert back to string
   $stavals = array_map('strval', $stavals);

}


print("$cb(\n/* output for $url follows: */\n");

// adding enclosing object with snr
print("{
snr: -1,
snrPhase: \"$snrPhase\",
phaseTimeIndex: $signalIndex,
data:\n");
print("[\n");

for($n=0; $n < count($stavals); $n++) {
   if($n < $nlines - 1 )
      print('[' . $xvals[$n] . ',' . $stavals[$n] . "],\n");
   else
      print('[' . $xvals[$n] . ',' . $stavals[$n] . "] \n");
}

print ("]}); ");// adding curly brace to close object




?>
