<?php

// emit a JSONP timeseries suitable for JavaScript caller to plot

// to invoke from command line and populate GET vars, use, eg:
//    php-cgi -f timeseriesAsJsonDev.php sta=ADK net=IU evdist=48.39 start=2010-02-26T20:31:26

include "remapper.php";     // remapping values to a new value range
include "snr.php";          // computing signal to noise ratio
include "peaks.php";        // smoothing and counting peaks 
include "TravelTimeLine.php";  // holds a line of tt webservice output

date_default_timezone_set("UTC");

// the (JSONP) JavaScript function that will be called by client
if(!isSet($_GET['callback']))
   $cb = 'callback';
else
   $cb = $_GET['callback'];

/*
var_dump($_GET);
if(isSet($argv)) {
    var_dump($argv);
    exit;
}
*/

// whether to search for a cached timeseries or not?
if(isSet($_GET['cacheFile']))
   $cacheFile = $_GET['cacheFile'];
   //$cacheFile="data/2842720/104.67.II.ALE.jsonp"; //example value
// todo: consider: cacheFile goes away, and use database to decide if to use cache
else { 
   $cacheFile = "";

   // start time in ISO (web services) fmt
   if(!isSet($_GET['start']))
       die("error: need start time as &start= " . __FILE__);
   else
      $start = $_GET['start'];

   // how many minutes of data, end time computed from start
   if(!isSet($_GET['mins']))
      $mins = '30';
   else
      $mins = $_GET['mins'];


   if(!isSet($_GET['net']))
       die("error: need network as &net= " . __FILE__);
   else
      $net = $_GET['net'];

   if(!isSet($_GET['sta']))
       die("error: need station as &sta= " . __FILE__);
   else
      $sta = $_GET['sta'];

   if(!isSet($_GET['chan']))
      $chan = 'BHZ';
   else
      $chan = $_GET['chan'];

   if(!isSet($_GET['loc']))
      $loc = '00';
   else
      $loc = $_GET['loc'];

   /* not currently needed/used
   if(!isSet($_GET['evid']))
      $evid = '-1';
   else
      $evid = $_GET['evid'];
   if(!isSet($_GET['evlat']))
       die("error: need station as &sta= " . __FILE__);
      $evlat = '00';
   else
      $evlat = $_GET['evlat'];
   if(!isSet($_GET['evlon']))
      $evlon = '00';
   else
      $evlon = $_GET['evlon'];
   */

   // for time travel call
   if(!isSet($_GET['evdep']))
       die("error: need event depth as &evdep= " . __FILE__);
   else
      $evdep = $_GET['evdep'];
   
   if(!isSet($_GET['evdist']))
       die("error: need station-event distance as &evdist= " . __FILE__);
   else
      $evdist = $_GET['evdist'];
   
   // future use:
   // minimum SNR to accept
   if(!isSet($_GET['minSNR']))
      $minSNR = 0; // unspecified, don't care
   else
      $minSNR = $_GET['minSNR'];
   
   // future use:  ??
   // whether to save to cache (filesystem and database) this timeseries
   if(!isSet($_GET['save']))
      $save = false;
   else
      $save = $_GET['save'];
   
   
   if(!isSet($_GET['deci'])) { // aka sps or Hz
      //$sps = 0.25;
      //$sps = 0.5;
      //$sps = 4;  // too high; maybe ok if about <=8 traces
      $sps = 1;
      $deci = "&deci=$sps"; // the complete URL param
   }
   else {
      $sps = 0; // 0 means don't specify deci= in URL
      $deci = "";
   }
   
   $sps = floatval($sps);

   if($sps !== 0 &&
      $sps !== 0.25 &&
      $sps !== 0.5 &&
      //$sps !== 0.75 && // not allowed
      $sps !== 1. &&
      $sps !== 2. &&
      $sps !== 4. ) {
         print("$cb(/* deci= must be 0.25, 0.5, 1, 2 or 4 - if passed */" . "[ ]);" );
         return;
      }
   } // end else, NOT using cacheFile 

// if useCache then check for json file and spit it out

if($cacheFile != "") {
    // todo: if caller didn't pass cachefile name we could try to construct it now
    // echo "spitting out $cacheFile\n";

    if(file_exists($cacheFile)) {
        //$rc = readfile($cacheFile);
        $dataAry = file($cacheFile);
        array_shift($dataAry);
        array_shift($dataAry);
        print("$cb(\n/* output for $cacheFile follows: */\n");
        print(implode("\n",$dataAry));

        exit;
     }
    else {
        //echo "oops. no such file\n";
        print("$cb(/* Requested cached file $cacheFile not found  */" . "[ ]);" );
        //exit;   // todo: don't I want to exit here?
        // for now, plow ahead, caller may have passed needed params to fetch data
     }
}

// normalize Y vals to +/- normrange/2
if(!isSet($_GET['normrange']))
    // normrange is "amplitude" in effect.
    // it how "wide/high" high spikes will be on recsec plot, in degs.
    // with denser (more traces) plots, then you'd lower this number.
    // logic for scaling values after drawn on plot exists in tsplotrev.html, as well.
    $normrange=15.; // bigger values than 1 cause some stations problems, e.g. RAR, SNZO
else
    $normrange = $_GET['normrange'];

if($normrange < 1. || $normrange > 100. ) {
  print("$cb(/* normrange must be between 1 and 100 if passed */" . "[ ]);" );
  return;
}

// half the new target range is positive, half negative:
$posrange = ($normrange > 0. ? floor($normrange / 2.) : 0.);
$negrange = - $posrange;

try {
    $startEpoch = strtotime($start); // will add time below
}
catch (Exception $e) {
    print("$cb(/* could not parse $start as valid date  */" . "[ ]);" );
    return;
}

$end = date('Y-m-d\TH:i:s', $startEpoch +  // want in UNIX seconds
    // number of minutes passed (currently 30) * 60 == secs requested:
    floor($mins * 60));

// ask for N seconds more up front than want, so can
// discard FFT-related wraparound spike at start of trace.
// this is far better than using a taper.
$fakeStart = date('Y-m-d\TH:i:s', $startEpoch - (30 * $sps));

//echo "start: $start , end: $end\n";
//exit;

// beware on tapering, it makes the trace fade out at both ends artificially
// at come decimations you get "wraparound" FFT artifact at start of trace (bad web services!)
// so if using tapering consider discarding the end of the trace (after asking for extra time at end)
$taper=0;  // test tapering? seems not wanted.

$url = "https://service.iris.edu/irisws/timeseries/1/query?" .
        "net=${net}&sta=${sta}&cha=${chan}&loc=${loc}${deci}&" .
        "demean=true&output=ascii2&taper=${taper}" .
        "&starttime=${fakeStart}&endtime={$end}";


/////////////////////////
// Turn off all error reporting
error_reporting(0);
try {
    $timeseries = file($url);
}
catch (Exception $e) {
  print("$cb(/* caught error getting timeseries: $url ," .  $e->getMessage() . "*/" .
        "[ ]);" );
  return;
}
// resume error reporting
error_reporting(E_ERROR | E_WARNING | E_PARSE);
/////////////////////////

if($timeseries === FALSE) {
  print("$cb(/* error getting timeseries for $url  */" .
        "[ ]);" );
  return;
}
// TIMESERIES 

if(!isSet($timeseries[0]) || strlen($timeseries[0]) < 50) {
  print("$cb(/* error getting timeseries: $url  */" .
        "[ ]);" );
  return;
}

// typical web service output:
// TIMESERIES IU_ANMO_00_BHZ_D, 7200 samples, 2 sps, 2010-02-27T06:30:00.019500, TSPAIR, FLOAT, COUNTS
// 2010-02-27T06:30:00.019500  -26567.449
// 2010-02-27T06:30:00.519500  -51493.125

// desire JSON format:
// callback(/* some comment or error info with URL used to attempt fetch */
// [
// [1268179200000,32.12],
// [1268265600000,32.21],
// [1268352000000,32.37],
// .....
// [1488844800000,139.52]
// ]);

// first get travel times so can measure SNR for the trace just fetched
$tturl = "http://service.iris.edu/irisws/traveltime/1/query?" .
        "distdeg=${evdist}&evdepth=${evdep}&phases=P,Pdiff,PP,PKIKP&mintimeonly=true&nodata=404";

/////////////////////////
// Turn off all error reporting
error_reporting(1);
try {
    $traveltimes = file($tturl);
}
catch (Exception $e) {
  print("$cb(/* error getting travel times: $tturl ," .  $e->getMessage() . "*/" .
        "[ ]);" );
  return;
}
error_reporting(E_ERROR | E_WARNING | E_PARSE);
/////////////////////////

if($traveltimes === FALSE) {
  print("$cb(/* error getting traveltimes: $tturl  */" .
        "[ ]);" );
  return;
}
if(!isSet($traveltimes[0]) || strlen($traveltimes[0]) < 8 ||
   strpos($traveltimes[0], "Model:") === FALSE || 
    count($traveltimes) < 5 ) {  // there are 4 header lines, see below
  print("$cb(/* error getting traveltimes: $tturl  */" .
        "[ ]);" );
  return;
}


// Choose which phase(s) at which to sample SNR

// Done by experimenting with this travel time URL at various depth params:
// https://service.iris.edu/irisws/traveltime/1/query?distdeg=2,10,20,32,50,55,90,95,96,97,98,99,100,101,102,103,104,105,107,110,111,112,113,114,115,120,125,130,138,144,152,158,171&evdepth=0&phases=P,PP,PKP,PKIKP&mintimeonly=true

$snrPhase = "";
$altSnrPhase = "";
$firstPhase = "";

// super close, the P and PP don't have enough separation for the time lengths I'm using for noise and signal
if($evdist >= 0. && $evdist <= 50) 
    $snrPhase = "P";
else
    if($evdist > 50. && $evdist <= 113.) {
        $snrPhase = "P";
        $altSnrPhase = "PP";
    }
    else
        if($evdist > 113. && $evdist <= 180.) {
            $snrPhase = "PKIKP";
            $altSnrPhase = "PP";
        }
        else {
            print("$cb(/* invalid event to station distance: $evdist  */" .
                  "[ ]);" );
            return;
        }
/*
// typical web service output:
Model: iasp91
Distance   Depth   Phase   Travel    Ray Param  Takeoff  Incident  Purist    Purist
  (deg)     (km)   Name    Time (s)  p (s/deg)   (deg)    (deg)   Distance   Name
-----------------------------------------------------------------------------------
    2.00    26.0   P         32.08    13.753     53.83    45.84     2.00   = P
    2.00    26.0   PP        39.60    13.754     53.83    45.84     2.00   = PP
  ......
  114.00    26.0   PKIKP   1116.46     1.915      6.45     5.73   114.00   = PKIKP
  114.00    26.0   PP      1170.60     7.090     24.59    21.70   114.00   = PP

  Use this table to see Shadow Zone:
  https://service.iris.edu/irisws/traveltime/1/query?distdeg=2,10,20,32,50,55,90,95,96,97,98,99,100,101,102,103,104,105,107,110,111,112,113,114,115,120,125,130,138,144,152,158,171&evdepth=26&phases=P,PP,PKIKP&mintimeonly=true

  Our plan:
  from   to   find SNR at phase
  ----------------------------
   0    98    P
  99   113    PP
  114  180    PKIKP
*/

// we checked it has > 5 elements already above
//var_dump($traveltimes);

// we ask for 3 phases, so output could contain all 3, but usually 1 or 2

$traveltimes = array_slice($traveltimes, 4);
//var_dump($traveltimes);

$snrSampleTimeSecs = -1; // flag val
$altSnrSampleTimeSecs = -1; // flag val


$ttlines = array();
foreach($traveltimes as $line) {
    list($jnk, $dist, $dep, $phase, $secs) = preg_split("/[\s,]+/", $line);
    $ttlines[] = new TravelTimeLine($dist, $dep, $phase, $secs);
} // end foreach traveltimes array

usort($ttlines, 'ttline_secs_cmp');


/*
foreach($traveltimes as $line) {

    list($jnk, $dist, $dep, $phase, $secs) = preg_split("/[\s,]+/", $line);
    //dbg echo("dist: $dist, depth:$dep, phase:$phase, secs:$secs \n") ;

    if($phase == $snrPhase ) {
    //dbg echo("got snrPhase " . $phase . " with secs: " . $secs . "\n") ;
        $snrSampleTimeSecs = $secs;
    }

    if($firstPhase == "") {
        //dbg echo("got firstPhase " . $phase . " with secs: " . $secs . "\n") ;
        $firstPhase = $phase;
        $firstIndex = round($secs * $sps);
    }   // what?? I don't get how this else works:
        // I think it makes no sense:
    else // if in shadow zone and there is a pdiff use it
        if($dist >= 104 && $dist <= 140 && $phase == "Pdiff") {
            $firstPhase = $phase;
            $firstIndex = round($secs * $sps);
        }
}
*/

//dbg echo "<br>SNR phase is: $snrPhase, alt SNR phase: $altSnrPhase  <br>";
foreach($ttlines as $ttline) {

//dbg echo("considering" . $ttline->phase . "...<br>\n") ;
    if($ttline->phase == $snrPhase ) {
//dbg echo("got snrPhase " . $ttline->phase . " with secs: " . $ttline->secs . "<br>\n") ;
        $snrSampleTimeSecs = $ttline->secs;
    }

    if($firstPhase == "") {
//dbg echo("got firstPhase " . $ttline->phase . " with secs: " . $ttline->secs . "<br>\n") ;
        $firstPhase = $ttline->phase;
        $firstIndex = round($ttline->secs * $sps);
    }   // what?? I don't get how this else works:
        // I think it makes no sense:
    /*
    else // if in shadow zone and there is a pdiff use it
        if($dist >= 104 && $dist <= 140 && $ttline->phase == "Pdiff") {
            $firstPhase = $ttline->phase;
            $firstIndex = round($ttline->secs * $sps);
        }
     */
}


/* NOTE: there might be only one phase arrival, the alternate */

// do the same for the alternate SNR smple phase
/*
foreach($traveltimes as $line) {
    list($jnk, $dist, $dep, $phase, $secs) = preg_split("/[\s,]+/", $line);
    if($phase == $altSnrPhase ) {
echo("got altSnrPhase " . $phase . " with secs: " . $secs . "\n") ;
        $altSnrSampleTimeSecs = $secs;
    }
}
 */

foreach($ttlines as $ttline) {
    if($ttline->phase == $altSnrPhase ) {
//dbg echo("got altSnrPhase " . $ttline->phase . " with secs: " . $ttline->secs . "<br>\n") ;
        $altSnrSampleTimeSecs = $ttline->secs;
    }
}









// if got no SNR phase at all, bail now, if saw only the alternate, transfer to primary

if($snrSampleTimeSecs == -1 && $altSnrSampleTimeSecs == -1)  { // we got neither phase
    print("$cb(/* failed to find travel time for either $snrPhase or $altSnrPhase using : $tturl  */" .
      "[ ]);" );
    return;
}
else 
    // we got at least one phase: 
    // if the only phase we got was the alternate (second), transfer it to primary (first)
    if($snrSampleTimeSecs == -1 && $altSnrSampleTimeSecs != -1 )  { 
        //list($jnk, $dist, $dep, $phase, $secs) = preg_split("/[\s,]+/", $line); // same ttline as above
        // $ttline still has last value from the previous ttlines loop above
//dbg echo "desired SNR phase $snrPhase not seen, using alternate : $altSnrPhase <br>\n";
        //$snrSampleTimeSecs = $ttline->secs; 
        $snrSampleTimeSecs = $altSnrSampleTimeSecs;
        $snrPhase = $altSnrPhase;
    }

//dbg echo "<br>";
//dbg echo "SNR phase: $snrPhase  at secs: $snrSampleTimeSecs<br>\n";
//dbg echo "alternate SNR phase: $altSnrPhase  at secs: $altSnrSampleTimeSecs<br>\n";






// now setup starting points in timeseries array for where SNR
// measurement(s) will begin and for how many samples
//
// noise: 120 seconds before arrival time, for 60 secs
$noiseIndex =   round($snrSampleTimeSecs * $sps - (120 * $sps));
$noiseLength =  round(60 * $sps);
// signal: lasts 60 seconds
$signalIndex =  round($snrSampleTimeSecs * $sps);
$signalLength = round(60 * $sps);

// at very very close stations, 0-2 degs, noise region may go negative and need clipping
if($noiseIndex < 0) {
    $noiseLength -=  noiseIndex;
    $noiseIndex = 0;
}
// very near stations could have noise overlap with signal zones
if($noiseIndex + $noiseLength > $signalIndex)
    $noiseLength = $signalIndex - $noiseIndex;

// now the same as above, for alt SNR sample phase, if present

if($altSnrSampleTimeSecs != -1) { // flag value
    $altNoiseIndex =   round($altSnrSampleTimeSecs * $sps - (120 * $sps));
    $altNoiseLength =  round(60 * $sps);
    $altSignalIndex =  round($altSnrSampleTimeSecs * $sps);
    $altSignalLength = round(60 * $sps);  
    if($altNoiseIndex < 0) {
        $altNoiseLength -=  altNoiseIndex;
        $altNoiseIndex =  0;
    }
    if($altNoiseIndex + $altNoiseLength > $altSignalIndex)
        $altNoiseLength = $altSignalIndex - $altNoiseIndex;
}


/*
echo("noise starts at element: $noiseIndex for $noiseLength elements\n");
echo("that is at ".round($noiseIndex/60.,2)." mins for ". $noiseLength/60 . " mins\n");
echo("signal starts at element: $signalIndex for: $signalLength elements\n");
echo("that is at ".round($signalIndex/60.,2)." mins for ". $signalLength/60 . " mins\n");

if($altSnrSampleTimeSecs != -1) {
    echo("altNoise starts at element: $altNoiseIndex for: $altNoiseLength elements\n");
    echo("that is at ".round($altNoiseIndex/60.,2)." mins for ". $altNoiseLength/60 . " mins\n");
    echo("altSignal starts at element: $altSignalIndex for: $altSignalLength elements\n");
    echo("that is at ".round($altSignalIndex/60.,2)." mins for ". $altSignalLength/60 . " mins\n");
}
*/



////////////////
// PROCESS timeseries
////////////////


array_shift($timeseries); // lose header line

$nlines = count($timeseries);

$xvals = array();
$stavals = array();

for($n=0; $n < $nlines; $n++) {
   $line = $timeseries[$n];
   $words = preg_split('/\s+/', $line);
   $ts = $words[0];
   $val = $words[1];

   try {
        $d = new DateTime($ts);  // ts = time string
   }
   catch (Exception $e) {
      // timeseries, if has a gap, will output a new header line, as in:
      // 2010-02-27T07:30:02.510600  450940.75
      // 2010-02-27T07:30:03.010600  232234.34
      // TIMESERIES IU_KMBO_00_BHZ_D, 651 samples, 2 sps, 2010-02-27T07:30:34.510600, TSPAIR, FLOAT, COUNTS
      // 2010-02-27T07:30:34.510600  -214537.28
      // 2010-02-27T07:30:36.010600  -482356.97

      //echo 'Caught exception: ',  $e->getMessage(), "\n";
      print("$cb(/* probable gap encountered in timeseries for $url  */" .  '[ ]);' );
      return;
   }

   $secs = intval($d->getTimestamp()); // get seconds since epoch

   // get ms portion, div by 1000 since web service considers the "." in string as a decimal point
   // if you DON'T do this you will have:
   //   secs: 1267252560  ms: 19500
   //   secs: 1267252560  ms: 519499
   //   secs: 1267252561  ms: 19500
   //   secs: 1267252561  ms: 519499

   $ms = (floatval($d->format('u')) / 1000.);
   // NOW you have:
   //   secs: 1267252560  ms: 19.5
   //   secs: 1267252560  ms: 519.499
   //   secs: 1267252561  ms: 19.5
   //   secs: 1267252561  ms: 519.499
   //   secs: 1267252562  ms: 19.5


   // drop unneeded precision, deps. on sps, this is for deci=2: want only 0 and 500 ms
   //$ms = ($ms < 500. ? 0 : 500); 

   // better to just round:
   $ms = round($ms);
   // NOW you have nice whole numbers:  (again for deci=2)
   //   secs: 1267252560  ms: 20
   //   secs: 1267252560  ms: 519
   //   secs: 1267252561  ms: 20
   //   secs: 1267252561  ms: 519

   //echo "secs: $secs  ms: $ms  val: $val\n";
   //if($n>5) exit;

   $epoch = $secs * 1000 + $ms; // highcharts wants milliseconds since epoch
   //$epoch = date("U", strtotime($ts)) * 1000.; // highcharts wants ms not secs

   // rcw: artifacts at start due to decimation.
   // see $fakeStart above
   // taper is one solution but tapers both ends, not just start.
   // so discard the samples between fakeStart and start
   // BE SURE to do this before measuring SNR below.
   if($secs >= $startEpoch) {
        $stavals[] = $val; // same as array_push
        $xvals[] = $epoch;
   }

} // end of timeseries processing loop


$posrange = ($normrange > 0. ? floor($normrange / 2.) : 0.);
$negrange = - $posrange;

// convert to float so can remap to new range
$stavals = array_map("floatval", $stavals);
//dbg print_r(array_slice($stavals, 0, 90));


// you MUST not count the first two values
// in max and min, UNLESS you are using taper=true
// as first two values are "bad".
// as in max(array_slice($stavals, 2 ));

$maxseen = max(array_slice($stavals,2));
$minseen = min(array_slice($stavals,2));
//var_dump($minseen);
//var_dump($maxseen);
//var_dump($negrange);
//var_dump($posrange);



// test: compute SNR BEFORE remapping or large spikes will reduce it greatly

// compute SNR, if have an alternate phase then use the max of the 2
$sigVals =   array_map('floatval', array_slice($stavals, $signalIndex, $signalLength));
$noiseVals = array_map('floatval', array_slice($stavals, $noiseIndex,  $noiseLength));
//dbg echo "signal-------------\n";
//dbg print_r($sigVals);
//dbg echo "noise-------------\n";
//dbg print_r($noiseVals);
$snr = round(snr($sigVals, $noiseVals));
//dbg echo "snr ";
//dbg print_r($snr);

$minOKSNR = 30; // prefer the first phase arrival

if($altSnrSampleTimeSecs != -1 && $snr < $minOKSNR ) { 
    // OK consider the alternate:
    $altSigVals =   array_map('floatval', 
                            array_slice($stavals, $altSignalIndex, $altSignalLength));
    $altNoiseVals = array_map('floatval', 
                            array_slice($stavals, $altNoiseIndex,  $altNoiseLength));
    //print_r($altSig);
    //echo "-------------\n";
    //print_r($altNoise);
    $altSnr = round(snr($altSigVals, $altNoiseVals));
//dbg echo "alt snr ";
//dbg print_r($altSnr);

    if($altSnr > $snr) {
//dbg print("the alternate SNR $altSnt is greater than SNR $snr, so using it\n");
       $sigVals = $altSigVals;
       $signalIndex = $altSignalIndex;
       $noiseVals = $altNoiseVals;
       $noiseIndex = $altNoiseIndex;
       $snr = $altSnr;
       $snrPhase = $altSnrPhase;
       $snrPhaseTimeIndex = $altSnrSampleTimeSecs;
    }
}

// abort now if SNR seems to low to be good

if($minSNR > 0 && $snr < $minSNR) {
  print("$cb(/* SNR: $snr not enough for minSNR: $minSNR : $url  */" .
        "[ ]);" );
  return;
}




// quality tweak: if max value in SNR chosen to measure at is significantly less
// than the max amplitude of the whole trace, remap (rescale) to gain amplitude, so
// can see the arrival we chose better
//
// for example compare IC.SSE here: 
//    http://ds.iris.edu/gsv/wsTracesForID.phtml?evid=5111620
// with no apparent P arrival ( though that is where SNR was taken and it was very good )
// to this plot scaled differently: 
//     http://service.iris.edu/irisws/timeseries/1/query?net=IC&sta=SSE&cha=BHZ&loc=00&deci=1&demean=true&output=plot&starttime=2015-04-25T06:11:25&duration=2000&height=650&width=800&antialiasing=true

if($snrPhase == "P") {
    // values have been remapped so grab new max/mins
    $maxseen = max(array_slice($stavals,2));
    $minseen = min(array_slice($stavals,2));
    $traceRange = abs($maxseen - $minseen);
    $sigRange = abs(max($sigVals) - min($sigVals));
    $snrPhaseVisibleRatio = round($traceRange / $sigRange);

//dbg print("SNR phase: $snrPhase,  trace range: $traceRange  sig range: $sigRange   ratio: $snrPhaseVisibleRatio <br> \n");
//dbg print("a high ratio means remap again, scale up so SNR phase is visible<br>\n");
//exit;

    // if our SNR phase is likely to NOT be very visible then maginfy trace:
    if($snrPhaseVisibleRatio > 15 ) {  // heuristically found
        //dbg print("magnifying trace vals by 6 but no change to snr calculated\n");
       for($v=0; $v<count($stavals); $v++)
           $stavals[$v] *= 6.;  // 6 is just a guess
    }
} // end snrPhase is P


// rcw: test: dampen: if in shadow zone, scale down the values to 
// avoid the Pdiff's showing up and confusing students

/*
if($dist >= 104. && $dist <= 140. && $firstPhase == "Pdiff") {
    $targetIndex = $firstIndex + round( 60 * $sps); // add a minute or two
    for($v=0; $v < $targetIndex; $v++) 
        $stavals[$v] /= 5.;  // 5 is an arbitrary value
}
 */





$r = new Remapper($minseen, $maxseen, $negrange, $posrange);

//var_dump($r);
//print($r->remap(100)."\n");

$stavals = array_map(array($r, 'remap'), $stavals);
//$stavals = array_map(function($v) { return round($v,4);},  $stavals);

//dbg echo "after remap:\n";
//dbg print_r(array_slice($stavals, 0, 90));
//dbg exit;

// our trace should be flatlining very near zero at start, unless very close to event.
//
// so sample and nudge the trace up or down towards zero, if needed
//
// problem is being off by 1 or 2, if drawn in a rec sec plot,
// means it is not anchored properly at the degree distance it belongs at,
// one that is a bit negative drawn just next to another that is
// a bit positive yields two traces literally on top of each other,
// which is not desirable. see RAR and SNZO w/Chile test
// also note this would not be much of a problem at typical degree gaps: ~10-15

if($dist >= 5) {  // very close to event ignore, noise sample is not reliable
    $avg = array_sum(array_slice($stavals, 5, 30)) / 30.;
    if($avg > 1.) {
//print( "avg of $avg means subtracting $avg from all stavals\n");
           for($s = 0; $s < count($stavals); $s++)
               $stavals[$s] -= $avg;
    }
    if($avg < -1.) {
//print( "avg of $avg means adding ".abs($avg)." to all stavals\n");
           for($s = 0; $s < count($stavals); $s++)
               $stavals[$s] += abs($avg);
    }
}

//print_r(array_slice($stavals, 0, 90));
//exit;


$smoothed = smooth($stavals, 230);  // window of samples at 1 sps
//echo "\n\nXXXX smoothed:\n";
//print_r($smoothed);

$peaks = getExtrema($smoothed);

//echo "\n\nXXXX peaks:\n";
//print_r($peaks);
//print("XXXX number of peaks: " . count($peaks) . "\n");
//print_r($smoothed);
// temp:
//$stavals = $smoothed;

$nsPeaks = count($peaks);

// convert back to string
$stavals = array_map('strval', $stavals); // todo: needed???

print("$cb(\n/* output for $url (traveltimes: $tturl ) follows: */\n");

// adding enclosing object with snr
print("{
\"snr\": $snr,
\"firstPhase\": \"$firstPhase\",
\"firstPhaseTimeIndex\": $firstIndex,
\"snrPhase\": \"$snrPhase\",
\"snrPhaseTimeIndex\": $signalIndex,
\"nsPeaks\": $nsPeaks,
\"data\":\n");
print("[\n");

$nvals = count($stavals);

// brittle bug fix alert:
// we now use 30 mins of data which is 1800 values;
// ~1/4 of the time the timeseries call produces 1801 values, due to decimation;
// this makes shadow zone show/hide break, so just use first 1800 values
//if($nvals > 1800) $nvals = 1800;

for($n=0; $n < 1800; $n++) {
    if($nvals == 1800 ) { // normal case
       if($n < $nvals - 1 )
          print('[' . $xvals[$n] . ',' . $stavals[$n] . "],\n");
       else
          print('[' . $xvals[$n] . ',' . $stavals[$n] . "] \n");
    }
    else {
        if($nvals > 1800) {
            if($n < 1799)
                print('[' . $xvals[$n] . ',' . $stavals[$n] . "],\n");
            else
                print('[' . $xvals[$n] . ',' . $stavals[$n] . "] \n");
        }
        else {
            if($n < $nvals)
                print('[' . $xvals[$n] . ',' . $stavals[$n] . "],\n");
            else {
                if(!isSet($tval) && $n == $nvals)
                    $tval = $xvals[$n-1];
                $tval += 1000;
                if($n < $nvals)
                    print('[' . $tval . ',' . $stavals[$n] . "],\n");
                else
                    if($n < 1799)
                        print('[' . $tval . ",0.],\n");
                    else
                        print('[' . $tval . ",0.]\n");
                }
            }
    }
}

print ("]}); ");// adding curly brace to close object

?>
