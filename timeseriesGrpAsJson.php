<?php

// goal is to reproduce the format of this file
//     https://www.highcharts.com/samples/data/jsonp.php?filename=activity.json&callback=?
// which is a multi dataset where the x values are all
// the same and then a number of y value groups (ie stations here)
// are included. this avoids repeating sending all the Xs,
// which are the same for all stations' timeseries.

// so, first station, get 2-column output; then
// for remaining just get 1-column as already have Xs

include "remapper.php";

date_default_timezone_set("UTC");

$startUrl = 'https://service.iris.edu/irisws/timeseries/1/query?net=IU&sta=';

//$end2Url = '&cha=BHZ&start=2010-02-27T06:30:00&end=2010-02-27T07:30:00&deci=1&output=ascii2&loc=00&demean=true&correct=true';
// use taper b/c if decimating, get bad values for first and second sample (spikes which become max and min vals)
$end2Url = '&cha=BHZ&start=2010-02-27T06:30:00&end=2010-02-27T06:31:00&deci=1&output=ascii2&loc=00&demean=true&taper=0.5';
$end1Url = '&cha=BHZ&start=2010-02-27T06:30:00&end=2010-02-27T06:31:00&deci=1&output=ascii&loc=00&demean=true&taper=0.5';

if(!isSet($_GET['sta']))
   //$sta = 'ANMO,COLA,MAJO,TUC,GUMO,KIEV,TATO,WAKE';
   $sta = 'ANMO';
else
   $sta = $_GET['sta'];

if(!isSet($_GET['callback']))
   $cb = 'callback';
else
   $cb = $_GET['callback'];

// normalize Y vals to +/1 normrange/2
if(!isSet($_GET['normrange']))
   $normrange = 0.; // dividing by 1 has no effect
else
   $normrange = $_GET['normrange'];

if($normrange < 0. || $normrange > 10000. )
   die("normrange must be positive  and <= 10000");

// half the new target range is positive, half negative:
$posrange = ($normrange > 0. ? floor($normrange / 2.) : 0.);
$negrange = - $posrange;

$stalist = explode(',' , $sta);
if($stalist === FALSE)
   die("must pass a station or other logic error");

$nsta = sizeof($stalist);
if($nsta == 0)
   die("must pass a station or other logic error");

//echo("$nsta stations passed");
//var_dump($stalist);


// get first station, with 2 column output so get Xs

$station = $stalist[0];
$url = $startUrl . $station . $end2Url;

$timeseries = file($url);

if($timeseries === FALSE) {
  print('callback(/* error in call to IRIS timeseries for $url  */' .
        '[ ]);' );
  exit(-1);
}

if(!isSet($timeseries[0]) || strlen($timeseries[0]) < 50) { // 50 is arb.
  print('callback(/* unrecognized timeseries header line for $url  */' .
        '[ ]);' );
  exit(-1);
}

if(count($timeseries) < 3) { // 3 is arb.
  print('callback(/* timeseries empty or too few lines for $url*/' .
        '[ ]);' );
  exit(-1);
}


// typical 2 col web service output:            sps controlled by "deci=" in URL TSPAIR=2 col   COUNTS=y units
// TIMESERIES IU_ANMO_00_BHZ_D, 7200 samples, 2 sps, 2010-02-27T06:30:00.019500, TSPAIR, FLOAT, COUNTS
// 2010-02-27T06:30:00.019500  -26567.449
// 2010-02-27T06:30:00.519500  -51493.125
// the ISO times must be parsed to UNIX epochs

// desire to output JSON format:
//  callback(
//   /* AAPL historical OHLC data from the Google Finance API */
//   {
//   "xData": [0.001567,0.011765,0.022194,0.032316,0.04266,0.063668.......],
//   "datasets": [{
//       "name": "ANMO",
//       "data": [13.833,12.524,11.441,10.651,9.961,4.566,.......],
//       "unit": "counts",
//       "type": "line",
//       "valueDecimals": 1           not sure, 3??
//  }, {
//       "name": "COLA",
//       "data": [26.857,27,27.111,27.2,27.2......
//      - - - - etc  - - - - - - - -
//       "valueDecimals": 0
//    }]
// });


// get the X vals

// ditch the header line
// we know we have more lines than 1, see above
array_shift($timeseries);
print_r(array_slice($timeseries, 0, 20));

$nSamps = count($timeseries);

$xvals = array(); // will be nSamps big w/ X vals
$stavals = array(); // (assoc) will have y vals for all stations

$stavals[$station] = array(); 

for($s=0; $s < $nSamps; $s++) {
   $line = $timeseries[$s];
   $words = preg_split('/\s+/', $line);  // split on 1+ spaces
   $time = $words[0];
   $val = $words[1];
   $epoch = date("U", strtotime($time)) * 1000.; // highcharts wants ms not secs

   $xvals[$s] = $epoch;
   array_push($stavals[$station], $val);
}

if($normrange > 0.) {
   
   //var_dump($stavals[$station]);
   //var_dump(array_map("floatval", $stavals[$station]));

//print_r(array_slice($stavals[$station], 0, 20));

   // convert to float so can remap to new range
   $stavals[$station] = array_map("floatval", $stavals[$station]);

   $maxseen = max($stavals[$station]);
   $minseen = min($stavals[$station]);
   //var_dump($minseen);
   //var_dump($maxseen);
   //var_dump($negrange);
   //var_dump($posrange);

   $r = new Remapper($minseen, $maxseen, $negrange, $posrange);
   //var_dump($r); 
   //print($r->remap(100)."\n");
   $stavals[$station] = array_map(array($r, 'remap'), $stavals[$station]);
   $stavals[$station] = array_map(function($v) { return round($v,2);},  $stavals[$station]);
   // convert back to string
   $stavals[$station] = array_map('strval', $stavals[$station]);
}


//print_r(array_slice($xvals, 0, 20));
//var_dump(array_slice($stavals[$station], 0, 20));

//  callback(
//   /* AAPL historical OHLC data from the Google Finance API */
//   {
//   "xData": [0.001567,0.011765,0.022194,0.032316,0.04266,0.063668.......],
//   "datasets": [{
//       "name": "ANMO",
//       "data": [13.833,12.524,11.441,10.651,9.961,4.566,.......],
//       "unit": "counts",
//       "type": "line",
//       "valueDecimals": 1           not sure, 3??
//  }, {
//       "name": "COLA",
//       "data": [26.857,27,27.111,27.2,27.2......
//      - - - - etc  - - - - - - - -
//       "valueDecimals": 0
//    }]
// });


if($nsta == 1) {
  print("$cb(\n/* output for $url follows: */\n");
  print("{\n");

  print('"xData": [');
  for($s=0; $s < $nSamps - 1; $s++) 
     print($xvals[$s] . ',');
  print($xvals[$s] . '],');
  print("\n");
  print('"datasets": [{');
  print("\n");

    print('  "name": "' . $station . '",');
    print("\n");

    print('  "data": [');
    for($s=0; $s < $nSamps - 1; $s++) 
       print($stavals[$station][$s] . ',');
    print($stavals[$station][$s] . '],');
    print("\n");

    print('  "unit": "counts",');
    print("\n");
    print('  "type": "line",');
    print("\n");
    print('  "valueDecimals": 4,');

    print("\n");
    if($nsta == 1) 
       print("}]\n");  // close this one station
    else
       print("}, {\n"); // leave open
}

if($nsta == 1) {
  print("})\n");  // close this entire dataset
  exit(0);
}

array_shift($stalist);
$nsta = sizeof($stalist);

// get rest of stations, with 1 column output, just Ys

foreach($stalist as $station) {

  // get just one column of data for the rest of them
  $url = $startUrl . $station . $end1Url;

  $timeseries = file($url);

  if($timeseries === FALSE) {
    print('callback(/* error in call to IRIS timeseries for $url  */' .
        '[ ]);' );
    exit(-1);
  }

  if(!isSet($timeseries[0]) || strlen($timeseries[0]) < 50) { // 50 is arb.
    print('callback(/* unrecognized timeseries header line for $url  */' .
        '[ ]);' );
    exit(-1);
  }

  if(count($timeseries) < 3) { // 3 is arb.
    print('callback(/* timeseries empty or too few lines for $url*/' .
        '[ ]);' );
    exit(-1);
  }
}

    //print($stavals[$station][$s] . '}],');

?>
