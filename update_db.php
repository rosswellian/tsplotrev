#!/usr/bin/php
<?php

// update an sqlite events database to have most recent new
// events from IRIS FDSN event web service. R.Welti

// can pass the db name and a time range to look back from now:

// $ update_db.php
//     or pass args
// $ update_db.php plotsdb tminus_2wks_ws


////////////////////////////////
$minmag = 7.0;
////////////////////////////////


require_once "TimeRanges.php";
require_once "longitude.php";

require_once "dbgmsg.php";
require_once "isset.php";

ini_set("memory_limit","500M");

// Set default timezone
date_default_timezone_set('UTC');

// Report simple running errors
error_reporting(E_ERROR | E_WARNING | E_PARSE);
// or:
// Report all but notice
error_reporting(E_ALL ^ E_NOTICE);

global $dbg;
$dbg = true; // override setting from dbgmsg.php

date_default_timezone_set("UTC");

$local_time = trim(`/bin/date`);

// tr is a TimeRange object:
$tr = new TimeRanges(gmmktime());

// the db to update:

$db_dir = "/opt/seismon/gsv";
if(isSet($argv[1]))
   $db_file = $argv[1];
else
   $db_file = "plotsdb";


print("$argv[0]: running at $local_time\n");

print("this script will fetch new events from event web service to the ($db_file) database.\n");
print("to fetch to another database it should be passed as arg 1, such as '$argv[0] plotsdb_tmp'\n\n");
print("after this run you should run getTraces.php, and scorePlots.php .\n\n");

print("you have 5 seconds to ctrl-C if you wish to cancel and pass a database name...");
sleep(5);


$scriptdir = "/opt/seismon/gsv/";
$outdir = $scriptdir . "/data";
if(!is_writable($outdir))
   die("Error: $outdir does not exist or can't write to it\n");

$userInfo = posix_getpwuid(posix_getuid());
$user = $userInfo['name'];
if($user != 'seismon')
   die("Error: script is to be run by user seismon \n");

if(!chdir($scriptdir))
   die("Error: script is to be run from $scriptdir\n");

///////////////////////////// get new events

// HOW far back will we query WS for events?

//various WS-formatted times, see TimeRange.php
//$evt_start = $tr->tminus_1day_ws;
//$evt_start = $tr->tminus_2days_ws;
//$evt_start = $tr->tminus_3days_ws;
//$evt_start = $tr->tminus_2wks_ws;
//$evt_start = $tr->tminus_1month_ws;
//$evt_start = $tr->tminus_1year_ws;

if(isSet($argv[2])) {
    if(strpos($argv[2], "tminus_") === FALSE)
      die ("error: args are db name and time range like tminus_2wks_ws");
	$evt_start = $tr->$argv[2];
}
else
	$evt_start = $tr->tminus_2days_ws;

// only look for events updated recently
$evt_updafter = $tr->tminus_2wks_ws;

print("\ngetting events after $evt_start with M>$minmag updated after $evt_updafter ...\n");

//dbgmsg("query evt_start: $evt_start");
//dbgmsg("query evt_updafter: $evt_updafter");

// make the URL and fetch it from WS

$url = "http://service.iris.edu/fdsnws/event/1/query?" .
       "starttime=$evt_start&updatedafter=$evt_updafter&format=text&nodata=404&minmag=$minmag";

dbgmsg("\nevents url: $url\n\n");

$evts = @file($url);   // "@" suppresses warning, typically 404 not found

if(sizeof($evts) <= 1) {
   print("Warning: could not get events with $url\nProbably no recent events or large enough mags. Exiting.\n");
   //print_r($evts);
   exit;
}

if(strpos($evts[0], "#EventID" ) === false) {
   echo("Error: did not recognize response as valid. Aborting.\n");
   print_r($evts);
   exit;
}

$evtsstr = print_r($evts, true);
$evtsstr = preg_replace("/\n\n/", "\n", $evtsstr);
dbgmsg('evts:' .$evtsstr);
//  3989393|2013-01-30T03:14:28|43.566|-127.598 |<lon360> |10.2|NEIC|NEIC PDE|NEIC PDE-Q||MB|5.3|NEIC|OFF COAST OF OREGON


///////////////////////////// connect to database

  $db_dsn =  "sqlite:$db_file";

  try {
    /*************************************
    * open connection                    *
    *************************************/
    echo "opening $db_dsn...\n";
    $file_db = new PDO($db_dsn);
    $file_db->setAttribute(PDO::ATTR_TIMEOUT, 5);  // 5 seconds
  }
  catch(PDOException $e) {
    // Print PDOException message
    echo "\nError: failed to open connection to events db\n";
    die($e->getMessage());
   }

   $file_db->setAttribute(PDO::ATTR_ERRMODE,
                          PDO::ERRMODE_EXCEPTION);
   $file_db->setAttribute(PDO::ATTR_TIMEOUT, 12);  // 12 seconds

   if($dbg) {
       try {
          $result = $file_db->query("PRAGMA table_info(events)");
       }
       catch(PDOException $e) {
          echo "\nError: failed to open connection to events db\n";
          die($e->getMessage());
       }
       var_dump($result);
       $cols = $result->fetchAll (PDO::FETCH_BOTH);
       $num = count ($cols);
       echo "Number of columns = " . $num . "\n\n";
       for ($i=0; $i<$num; $i++) {
          $numfld = count ($cols[$i]);
          for ($j=0; $j<$numfld; $j++) {
             echo $cols[$i][$j] . " ";
             }
          echo "\n";
       }
   }


   // record # events in table before loop; compare after loop

   $result = $file_db->query("SELECT count(*)  FROM events ");
   $cols = $result->fetchAll (PDO::FETCH_BOTH);
   $evcount_pre = intval($cols[0][0]);

   dbgmsg("there are " . $evcount_pre  .  " events in events table before update");

   $evquery = 'replace into events values (' .
                   ':evid, ' .
                   ':time, ' .
                   ':lat,' .
                   ':lon,' .
                   ':lon360,' .
                   ':depth, ' .
                   ':author, ' .
                   ':catalog, ' .
                   ':contrib, ' .
                   ':contribID, ' .
                   ':magType, ' .
                   ':mag, ' .
                   ':magAuthor, ' .
				   ':region )';

    $stmt = $file_db->prepare($evquery);


///////////////////////////// main "update" loop ///////////////

    array_shift($evts); // lose col header line

    // this busy file could be used to tell index.php
    // (web server) to use the backup right now

    if(isSet($busyfile) && file_exists($busyfile))
       unlink($busyfile);
    $busyfile = "$outdir/update.busy";
    touch($busyfile);

    $result = $file_db->query("BEGIN transaction");

    $e=0;
    foreach($evts as $evt) {
       $e++;

       list($eventid,
            $time,
            $eventlat,
            $eventlon,
            $depth,
            $author,
            $catalog,
            $contributor,
            $contributorID,
            $magType,
            $mag,
            $magAuthor,
            $region
        ) = explode('|',rtrim($evt));  // ditch newline

       /*print("id $eventid,
            $time,
            $eventlat,
            $eventlon,
            360 $eventlon360,
            $depth,
            $author,
            cat $catalog,
            $contributor,
            $contributorID,
            magtype $magType,
            $mag,
            region $magAuthor,
            $region\n");
*/

       if($mag == "") {
          if($dbg) print("\nignored a null mag \n");
          //$mag = "0";
          continue;
       }
       if($depth == "") {
          if($dbg) print("\nignored a null depth \n");
          //$depth = "0";
          continue;
       }
       if($contributor == "") {
          if($dbg) print("\ngave a null contributor unk\n");
          $contributor = "unk";
       }
       if($author == "") {
          if($dbg) print("\ngave a null author unk\n");
          $author = "unk";
       }
       if($magAuthor == "") {
          if($dbg) print("\ngave a null magAuthor unk\n");
          $magAuthor = "unk";
       }
       if($contributorID == "") {
          if($dbg) print("\ngave a null contribID unk\n");
          $contributorID = "unk";
       }
       if($region == "") {
          if($dbg) print("\ngave a null region unk\n");
          $region = "unk";
       }

       /////////////// update database

       $eventlon360 = lon180To360($eventlon);

       try {

           if($dbg)  {
              print("binding and will execute something close to:\n" .
                   "replace into events values (" .
                   "   $eventid,\n " .
                   "   $time,\n " .
                   "   $eventlat," .
                   "   $eventlon," .
                   "   $eventlon360," .
                   "   $depth,\n " .
                   "   $author,\n " .
                   "   $catalog, " .
                   "   $contributor, " .
                   "   $contributorID, " .
                   "   $magType, " .
                   "   $mag, " .
                   "   $magAuthor, " .
                   "   $region, \n");

               //$stmt->debugDumpParams();
           }

           // Bind values to statement variables
           $stmt->bindValue(':evid', $eventid, PDO::PARAM_STR);
           $stmt->bindValue(':time', $time, PDO::PARAM_STR);
           $stmt->bindValue(':lat', $eventlat, PDO::PARAM_STR);
           $stmt->bindValue(':lon', $eventlon, PDO::PARAM_STR);
           $stmt->bindValue(':lon360', $eventlon360, PDO::PARAM_STR);
           $stmt->bindValue(':depth', $depth, PDO::PARAM_STR);
           $stmt->bindValue(':author', $author, PDO::PARAM_STR);
           $stmt->bindValue(':catalog', $catalog, PDO::PARAM_STR);
           $stmt->bindValue(':contrib', $contributor, PDO::PARAM_STR);
           $stmt->bindValue(':contribID', $contributorID, PDO::PARAM_STR);
           $stmt->bindValue(':magType', $magType, PDO::PARAM_STR);
           $stmt->bindValue(':mag', $mag, PDO::PARAM_STR);
           $stmt->bindValue(':magAuthor', $magAuthor, PDO::PARAM_STR);
           $stmt->bindValue(':region', $region, PDO::PARAM_STR);


           // Execute statement
           $stmt->execute();
     }
     catch(PDOException $e) {
         // Print PDOException message
         echo "\nError: failed to bind and execute replace into statement\n";
         echo($e->getMessage());
         if(isSet($busyfile) && file_exists($busyfile))
            unlink($busyfile);
         // die($e->getMessage());
         // or
         echo "Continuing.\n";
     }


/*  //////////////////////////////
    OR: choose this method instead: which is about as fast and easier to debug:

     try {
       // Execute statement
       echo "execute...\n";
       //$stmt->execute();
       echo "back from execute\n";

       $evquery = "replace into events values (" .
                   " $eventid , " .
                   "'$time', " .
                   " $eventlat ," .
                   " $eventlon ," .
                   " $eventlon360 ," .
                   " $depth, " .
                   "'$author', " .
                   "'$catalog', " .
                   "'$contrib', " .
                   "'$contribID', " .
                   "'$magType', " .
                   " $mag, " .
                   "'$magAuthor', " .
                   "'$region' )";

       $dbg && print("query:\n$evquery\n");

       $stmt = $file_db->prepare($evquery);

       // Execute statement
       $stmt->execute();

     }
     catch(PDOException $e) {
       // Print PDOException message
       echo "\nError: failed to execute query; continuing \n";
       echo($e->getMessage());
       //if(isSet($busyfile) && file_exists($busyfile))
          //unlink($busyfile);
       //die($e->getMessage());
      }
////////////////// /////////////////   */

   } // end foreach event

   try {
       $result = $file_db->query("COMMIT transaction");
     }
   catch(PDOException $e) {
       // Print PDOException message
       echo "\nError: failed to commit transaction\n";
       echo($e->getMessage());
       if(isSet($busyfile) && file_exists($busyfile))
          unlink($busyfile);
       $file_db = null; // close db
       die($e->getMessage());
      }

   // this busy file could be used to tell index.php (web server) that we are finished
   if(isSet($busyfile) && file_exists($busyfile))
      unlink($busyfile);

   $result = $file_db->query("SELECT count(*)  FROM events ");
   $cols = $result->fetchAll (PDO::FETCH_BOTH);
   $evcount_post = intval($cols[0][0]);

   print("event count before update: $evcount_pre ; after update: $evcount_post \n");

   if($evcount_post >= $evcount_pre)
      $newevents = ($evcount_post - $evcount_pre);
   else
      $newevents = "warning: event counting logic error";

   print("new events this update: $newevents\n");

   $file_db = null; // close db

   $local_time = trim(`/bin/date`);
   print("update_db.php finished at $local_time with $newevents new events\n");
   exit(0);

?>
